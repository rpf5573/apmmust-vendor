const { join, resolve } = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: join(__dirname, '../src/index.tsx'),
  devtool: 'eval-source-map',
  output: {
    filename: 'main.js',
    path: join(__dirname, '../dist'),
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/, // .ts파일을 만나면
        exclude: /node_modules/, // node_modules를 제외하고
        use: ['babel-loader'], // babel-loader에게 일을 맡긴다!
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader', 'postcss-loader'],
      },
      {
        test: /\.(png|jpg|jpeg)$/i,
        type: 'asset/resource', // asset/resource는 webpack5 에서 url-loader, file-loader대신 지원하는 기능입니다
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: join(__dirname, '../public/index.html'),
    }),
    new CleanWebpackPlugin(),
  ],
  resolve: {
    extensions: ['.tsx', '.ts', '.jsx', '.js'],
    alias: {
      '@root': resolve(__dirname, '../'),
      '@src': resolve(__dirname, '../src'),
      '@shared': resolve(__dirname, '../src/components/@shared'),
      '@components': resolve(__dirname, '../src/components'),
      '@custom-types': resolve(__dirname, '../src/custom-types/index.ts'),
      '@pages': resolve(__dirname, '../src/pages'),
      '@assets': resolve(__dirname, '../src/assets'),
      '@utils': resolve(__dirname, '../src/utils'),
      '@constants': resolve(__dirname, '../src/constants.ts'),
      '@api': resolve(__dirname, '../src/api'),
      '@auth': resolve(__dirname, '../src/auth'),
      '@context': resolve(__dirname, '../src/context'),
      '@add-product-page': resolve(__dirname, '../src/pages/add-product-page'),
      '@edit-product-page': resolve(__dirname, '../src/pages/edit-product-page'),
      '@login-page': resolve(__dirname, '../src/pages/login-page'),
      '@my-account-page': resolve(__dirname, '../src/pages/my-account-page'),
      '@hooks': resolve(__dirname, '../src/hooks'),
      '@hoc': resolve(__dirname, '../src/hoc'),
      '@global-state': resolve(__dirname, '../src/global-state'),
    },
  },
  cache: {
    type: 'filesystem',
    buildDependencies: {
      config: [__filename],
    },
  },
};
