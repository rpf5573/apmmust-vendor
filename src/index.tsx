import { API_URL } from '@constants';
import { ConfigProvider } from 'antd-mobile';
import koKR from 'antd-mobile/es/locales/ko-KR';
import { createRoot } from 'react-dom/client';

import App from './App';
import './tailwind.css';

const root = document.getElementById('root');
if (root) {
  createRoot(root).render(
    <ConfigProvider locale={koKR}>
      <App />
    </ConfigProvider>,
  );
}
