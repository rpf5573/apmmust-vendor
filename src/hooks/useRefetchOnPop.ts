import { useActivity } from '@stackflow/react';
import { UseQueryResult } from '@tanstack/react-query';
import { useEffect } from 'react';

import useLoading from './useLoading';

const useRefetchOnPop = (query: UseQueryResult) => {
  const activity = useActivity();
  const setGlobalLoading = useLoading(query.isLoading);
  useEffect(() => {
    setGlobalLoading(true);
    if (activity.isTop) {
      query.refetch().finally(() => {
        setGlobalLoading(false);
      });
    }
  }, [activity.isTop]);

  // 로딩화면 보여주기
  return query.isFetching;
};

export default useRefetchOnPop;
