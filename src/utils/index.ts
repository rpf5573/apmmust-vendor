import { Gender, Term, TreeTerm } from '@custom-types';

export const isNull = (value: unknown): value is null => value === null;

export const isNullOrUndefined = (value: unknown): value is null | undefined => value === undefined || value === null;

export const isBoolean = (value: unknown): value is boolean => typeof value === 'boolean';

export const isString = (value: unknown): value is string => typeof value === 'string';

export const isNumber = (value: unknown): value is number => typeof value === 'number';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const isFunction = (val: unknown): val is (...args: any) => any => typeof val === 'function';

export const isObject = <T extends object>(value: unknown): value is T =>
  typeof value === 'object' && !isNullOrUndefined(value) && !Array.isArray(value);

export const isArray = (value: unknown): value is Array<unknown> => Array.isArray(value);

export const hasOwnProperty = <ObjectType extends object, KeyType extends PropertyKey>(
  obj: ObjectType,
  prop: KeyType,
): obj is ObjectType & Record<KeyType, unknown> => obj.hasOwnProperty(prop);

export const flattenTreeStructureTermArray = <T extends TreeTerm>(array: Array<T>, depth: number = 0): Array<T> => {
  let result: Array<T> = [];

  for (const item of array) {
    const { children } = item;
    result.push({
      ...item,
      depth,
    } as T);

    if (children) {
      result = result.concat(flattenTreeStructureTermArray(children, depth + 1) as Array<T>);
    }
  }

  return result;
};

export const filterCategoryByGender = (categories: Array<TreeTerm>, gender: Gender = 'c-unisex') => {
  if (gender === 'a-men') {
    return categories.filter(category => category.slug === 'men' || category.slug === 'accessories');
  }
  if (gender === 'b-women') {
    return categories.filter(category => category.slug === 'women' || category.slug === 'accessories');
  }
  if (gender === 'c-unisex') {
    return categories.filter(
      category => category.slug === 'men' || category.slug === 'women' || category.slug === 'accessories',
    );
  }

  return [];
};

export const generateProductFilename = (file: File, vendor: string) => {
  const date = new Date();
  const year = date.getFullYear();
  const month = String(date.getMonth() + 1).padStart(2, '0');
  const day = String(date.getDate()).padStart(2, '0');

  const filename = `product/${year}/${month}/${day}/${vendor}/${file.name}`;
  return filename;
};

export const transformGender = (gender: string) => {
  let newGender = null;
  if (gender === 'men') {
    newGender = 'a-men';
  }
  if (gender === 'women') {
    newGender = 'b-women';
  }
  if (gender === 'unisex') {
    newGender = 'c-unisex';
  }
  if (newGender === null) {
    throw new Error('gender에 이상한 값이 들어왔습니다');
  }
  return newGender;
};
