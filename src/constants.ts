export const PATH = {
  MAIN: '/',
  LOGIN: '/login',
  ADD_PRODUCT: '/add-product',
} as const;

export const API_ERROR = {
  EXPIRED_REFRESH_TOKEN: {
    CODE: 4001,
    MESSAGE: '오류가 발생했습니다 :(',
  },
};

export const ROOT_URL = 'https://test-apmmust.store';
export const API_URL = `${ROOT_URL}/wp-json/apmmust-vendor-server/v1`;
export const API_URL_V2 = `${ROOT_URL}/wp-json/apmmust-vendor-server/v2`;
export const COMMA = ',';

// @TODO: 에러 메세지 관리 다시 해야겠다
export const ERROR_CODE: Record<string, string> = {
  '[jwt_auth] invalid_username': '이메일을 다시 확인해주세요',
  '[jwt_auth] empty_username': '이메일을 입력해주세요',
  '[jwt_auth] incorrect_password': '비밀번호를 다시 확인해주세요',
  '[jwt_auth] empty_password': '비밀번호를 입력해주세요',
  not_yet_approved: '관리자의 승인을 검토중입니다',
};

export const UNKNOWN_ERROR = '알수없는 오류가 발생했습니다';

export const FABRICS = [
  { label: '면', value: 'cotton' },
  { label: '폴리에스테르', value: 'polyester' },
  { label: '나일론', value: 'nylon' },
  { label: '레이온', value: 'rayon' },
  { label: '울', value: 'wool' },
  { label: '아크릴', value: 'acrylic' },
  { label: '린넨', value: 'linen' },
  { label: '스판', value: 'spandex' },
  { label: '폴리우레탄', value: 'polyurethane' },
  { label: '가죽', value: 'leather' },
  { label: '캐시미어', value: 'cashmere' },
  { label: '모달', value: 'modal' },
];
