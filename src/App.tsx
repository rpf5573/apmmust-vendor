import { QueryClient, QueryClientProvider, useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import {
  Button,
  CheckList,
  DatePicker,
  Dialog,
  Dropdown,
  Form,
  Input,
  Popup,
  Radio,
  SearchBar,
  Selector,
  Slider,
  Space,
  Stepper,
  Switch,
  TextArea,
} from 'antd-mobile';
import { Suspense, useMemo, useState } from 'react';
import { ErrorBoundary } from 'react-error-boundary';
import { Toaster } from 'react-hot-toast';
import { RouterProvider, createBrowserRouter } from 'react-router-dom';

import AddProduct from '@pages/add-product-page/AddProduct';
import Home from '@pages/home/Home';
import SignUp from '@pages/sign-up/SignUp';

import GlobalErrorView from '@components/@shared/GlobalErrorView';
import GlobalLoading from '@components/@shared/GlobalLoading';

import './App.css';
import Login from './pages/login/Login';

// Create a client
const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: 0,
      staleTime: 0,
      gcTime: 0,
      throwErrors: true,
      suspense: false,
    },
  },
});

const router = createBrowserRouter([
  {
    path: '/*',
    element: <Home />,
  },
  {
    path: '/login',
    element: <Login />,
  },
  {
    path: '/sign-up',
    element: <SignUp />,
  },
]);

function App() {
  return (
    <ErrorBoundary FallbackComponent={GlobalErrorView}>
      <QueryClientProvider client={queryClient}>
        <RouterProvider router={router} />
        <Toaster />
      </QueryClientProvider>
    </ErrorBoundary>
  );
}

export default App;
