export type TermMap = Record<string, Term>;

export type Term = {
  term_id: number;
  name: string;
  parent: number;
  count?: number;
  description?: string;
  slug?: string;
};

export type TreeTerm = {
  children?: Array<TreeTerm>;
  depth: number;
} & Term;

export type CategoryTerm = { gender: Gender } & TreeTerm;

export type MixingRate = Record<string, string | number>;

export type Product = {
  id: string;
  name: string;
  regular_price: string;
  sale_price?: string;
  weight?: string;
  sizes?: Array<Term>;
  colours?: Array<Term>;
  featured_image_url: string;
  gallery_image_urls?: Array<string>;
  genders?: Array<Term>;
  categories?: Array<Term>;
  mixing_rates?: Array<MixingRate>;
  seasons?: Array<Term>;
  made_ins?: Array<Term>;
  sku: string;
};

// 앞에 a, b, c를 붙이는 이유는, 이상하게 ant design Selector 컴포넌트에서 women을 men으로 취급한다
export type Gender = 'a-men' | 'b-women' | 'c-unisex' | null;

export type Notice = {
  id: string;
  title: string;
  content: string;
  created_at: string;
};
