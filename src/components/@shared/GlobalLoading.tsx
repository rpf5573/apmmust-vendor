import { Mask, SpinLoading } from 'antd-mobile';

type GlobalLoadingProps = {
  visible: boolean;
};

const GlobalLoading: React.FC<GlobalLoadingProps> = ({ visible }) => {
  if (!visible) return null;
  return (
    <div className="loading-container">
      <Mask visible={true}>
        <SpinLoading color="white" />
      </Mask>
      <style jsx>{`
        .loading-container :global(.adm-mask) {
          display: flex;
          justify-content: center;
          align-items: center;
        }
        .content {
          width: 50vw;
          height: 50vw;
          background-color: white;
          display: flex;
          justify-content: center;
          align-items: center;
          flex-direction: column;
          row-gap: 20px;
        }
      `}</style>
    </div>
  );
};

export default GlobalLoading;
