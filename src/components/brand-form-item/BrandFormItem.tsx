import useGetProductTerms from '@api/api-terms';
import { Term } from '@custom-types';
import { flattenTreeStructureTermArray } from '@utils';
import { Button, CheckList, Form, Input, Popup, SearchBar, Space, Tag } from 'antd-mobile';
import { FormInstance } from 'rc-field-form';
import { useMemo, useState } from 'react';
import _ from 'underscore';

type BrandFormItemProps = {
  form: FormInstance;
};

const BrandFormItem: React.FC<BrandFormItemProps> = ({ form }) => {
  const getTermsQuery = useGetProductTerms();
  const [visible, setVisible] = useState(false);
  const [selected, setSelected] = useState<Array<string>>([]);
  const [searchText, setSearchText] = useState('');

  const brands = getTermsQuery.data?.data.brands ?? [];
  const brandMap = getTermsQuery.data?.data.brand_map ?? {};

  const brandNamesForSearch = useMemo(() => {
    return brands.map(item => item.name.toLocaleLowerCase().replace(/\s+/g, ''));
  }, [brands]);

  const filteredItems = useMemo(() => {
    return searchText
      ? brands.filter((_, index) =>
          brandNamesForSearch[index].includes(searchText.toLocaleLowerCase().replace(/\s+/g, '')),
        )
      : brands;
  }, [brands, searchText]);

  if (brands.length === 0) return null;

  const handleBrandChange = (val: Array<string>) => {
    const term_id = val[0];
    setSelected([term_id]);
    setVisible(false);
    form.setFieldValue('brand', term_id);
  };

  return (
    <Form.Item label="브랜드" name="brand" rules={[{ required: true, message: '브랜드를 입력해주세요!' }]}>
      <div>
        <Space align="center">
          <Button
            onClick={() => {
              setVisible(true);
            }}
          >
            선택
          </Button>
          <div className="flex gap-x-1">
            {selected.map(term_id => {
              const brand_name = brandMap[term_id]?.name;
              return (
                <Tag key={term_id} color="primary" fill="outline" className="text-[14px]">
                  {brand_name}
                </Tag>
              );
            })}
          </div>
        </Space>
        <Popup
          visible={visible}
          onMaskClick={() => {
            setVisible(false);
          }}
          destroyOnClose
        >
          <div className="p-[12px]">
            <SearchBar
              placeholder="브랜드 검색"
              value={searchText}
              onChange={v => {
                setSearchText(v);
              }}
            />
          </div>
          <div>
            <CheckList defaultValue={[...selected]} onChange={handleBrandChange}>
              {filteredItems.map(({ term_id, name }) => (
                <CheckList.Item key={term_id} value={`${term_id}`}>
                  {name}
                </CheckList.Item>
              ))}
            </CheckList>
          </div>
        </Popup>
      </div>
    </Form.Item>
  );
};

export default BrandFormItem;
