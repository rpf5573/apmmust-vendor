import { postMyAccount, useGetMyAccount } from '@api/api-my-account';
import useGetProductTerms from '@api/api-terms';
import withAuth from '@hoc/withAuth';
import useLoading from '@hooks/useLoading';
import useRefetchOnPop from '@hooks/useRefetchOnPop';
import { AppScreen } from '@stackflow/plugin-basic-ui';
import { message } from 'antd';
import { Button, List } from 'antd-mobile';
import { atom } from 'jotai';
import { useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';

import BankFormItem, { bank_form_item_name } from './edit-form-item/Bank';
import BrandFormItem, { brand_form_item_name } from './edit-form-item/Brand';
import BuildingFormItem from './edit-form-item/Building';
import DepositAccountFormItem, { deposit_account_form_item_name } from './edit-form-item/DepositAccount';
import EmailFormItem, { email_form_item_name } from './edit-form-item/Email';
import FloorFormItem from './edit-form-item/Floor';
import PasswordFormItem, { password_1_form_item_name, password_2_form_item_name } from './edit-form-item/Password';
import PhoneNumberFormItem, { phone_number_form_item_name } from './edit-form-item/PhoneNumber';
import RoomNumberFormItem from './edit-form-item/RoomNumber';
import { useFlow } from './stackflow';

export const atom_building = atom<string | null>(null);
export const atom_floor = atom<string | null>(null);
export const atom_roomNumber = atom<string | null>(null);

const EditMyAccount: React.FC = () => {
  const form = useForm();
  const { pop } = useFlow();

  const getMyAccountQuery = useGetMyAccount();
  const brandId = getMyAccountQuery.data?.data?.brand_id ?? '0';
  const getProductTermsQuery = useGetProductTerms();
  const setGlobalLoading = useLoading(false);

  const isGetMyAccountLoading = useRefetchOnPop(getMyAccountQuery);
  const isGetProductTermsLoading = useRefetchOnPop(getProductTermsQuery);
  if (isGetMyAccountLoading || isGetProductTermsLoading) return null;

  const myAccountData = getMyAccountQuery.data!.data;
  const terms = getProductTermsQuery.data!.data;

  const onSubmit = (data: any) => {
    const requiredProperties = [
      email_form_item_name,
      password_1_form_item_name,
      password_2_form_item_name,
      brand_form_item_name,
      bank_form_item_name,
      deposit_account_form_item_name,
      phone_number_form_item_name, // building, floor, store는 brand로 부터 서버에서 알아서 추출함
    ];
    // 이걸 프론트에서 해주는게 맞나...?
    const hasAllProperties = requiredProperties.every(property => data.hasOwnProperty(property));
    if (!hasAllProperties) {
      message.error('필수 항목을 모두 입력해주세요.');
      return;
    }

    setGlobalLoading(true);
    postMyAccount(data)
      .then(res => {
        message.success(res.message);
        pop();
      })
      .catch(err => {
        message.error(err.message);
      })
      .finally(() => {
        setGlobalLoading(false);
      });
  };

  return (
    <AppScreen appBar={{ title: '정보 수정' }}>
      <FormProvider {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)} className="adm-form mb-10">
          <List>
            <EmailFormItem value={myAccountData.email} />
            <PasswordFormItem />
            <BrandFormItem brandId={brandId} terms={terms.brands} termMap={terms.brand_map} />
            <BuildingFormItem value={myAccountData.building} />
            <FloorFormItem value={myAccountData.floor} />
            <RoomNumberFormItem value={myAccountData.room_number} />
            <BankFormItem value={myAccountData.bank} />
            <DepositAccountFormItem value={myAccountData.deposit_account} />
            <PhoneNumberFormItem value={myAccountData.phone_number} />
          </List>
          <div className="adm-form-footer">
            <Button block type="submit" color="primary" size="large">
              수정완료
            </Button>
          </div>
        </form>
      </FormProvider>
    </AppScreen>
  );
};

export default withAuth(EditMyAccount);
