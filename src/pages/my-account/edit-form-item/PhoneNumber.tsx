import { Input } from 'antd-mobile';
import { Controller, useFormContext } from 'react-hook-form';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

export const phone_number_form_item_name = 'phone_number';

type PhoneNumberFormItemProps = {
  value: string;
};

const PhoneNumberFormItem: React.FC<PhoneNumberFormItemProps> = ({ value: phoneNumber }) => {
  const form = useFormContext();

  return (
    <Controller
      name={phone_number_form_item_name}
      control={form.control}
      defaultValue={phoneNumber}
      render={({ field: { onChange, value, name }, fieldState: { invalid, error } }) => (
        <CustomFormItem label="전화번호" showError={invalid} errorMessage={error?.message}>
          <Input value={value} onChange={onChange} />
        </CustomFormItem>
      )}
    />
  );
};

export default PhoneNumberFormItem;
