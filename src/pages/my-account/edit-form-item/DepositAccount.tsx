import { Input } from 'antd-mobile';
import { Controller, useFormContext } from 'react-hook-form';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

export const deposit_account_form_item_name = 'deposit_account';

type DepositAccountFormItemProps = {
  value: string;
};

const DepositAccountFormItem: React.FC<DepositAccountFormItemProps> = ({ value: depositAccount }) => {
  const form = useFormContext();

  return (
    <Controller
      name={deposit_account_form_item_name}
      control={form.control}
      defaultValue={depositAccount}
      render={({ field: { onChange, value, name }, fieldState: { invalid, error } }) => (
        <CustomFormItem label="계좌번호" showError={invalid} errorMessage={error?.message}>
          <Input value={value} onChange={onChange} />
        </CustomFormItem>
      )}
    />
  );
};

export default DepositAccountFormItem;
