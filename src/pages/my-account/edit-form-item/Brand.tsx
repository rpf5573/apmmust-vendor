import { getBrandInfo } from '@api/api-brand-info';
import { ApiTerms } from '@api/api-terms';
import useLoading from '@hooks/useLoading';
import { Button, CheckList, Popup, SearchBar, Space, Tag } from 'antd-mobile';
import { useSetAtom } from 'jotai';
import { useEffect, useMemo, useState } from 'react';
import { Controller, useFormContext, useWatch } from 'react-hook-form';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

import { atom_building, atom_floor, atom_roomNumber } from '../EditMyAccount';

export const brand_form_item_name = 'brand';

type BrandFormItemProps = {
  brandId: string;
  terms: ApiTerms['get']['responseData']['data']['brands'];
  termMap: ApiTerms['get']['responseData']['data']['brand_map'];
};

const BrandFormItem: React.FC<BrandFormItemProps> = ({ brandId, terms: brands, termMap: brandMap }) => {
  const form = useFormContext();
  const setBuilding = useSetAtom(atom_building);
  const setFloor = useSetAtom(atom_floor);
  const setRoomNumber = useSetAtom(atom_roomNumber);
  const setGlobalLoading = useLoading(false);

  const [visible, setVisible] = useState(false);
  const [selected, setSelected] = useState<string[]>([]);

  const [searchText, setSearchText] = useState('');

  const brandNamesForSearch = useMemo(() => {
    return brands.map(item => item.name.toLocaleLowerCase().replace(/\s+/g, ''));
  }, [brands]);

  const filteredItems = useMemo(() => {
    return searchText
      ? brands.filter((_, index) =>
          brandNamesForSearch[index].includes(searchText.toLocaleLowerCase().replace(/\s+/g, '')),
        )
      : brands;
  }, [brands, searchText]);

  // selected가 바뀌면 form에도 업데이트
  useEffect(() => {
    form.setValue(brand_form_item_name, selected);
  }, [selected]);

  // 초기값 설정
  useEffect(() => {
    setSelected([`${brandId}`]);
  }, [brandId]);

  const handleBrandChange = (val: Array<string>) => {
    setGlobalLoading(true);
    const term_id = val[0];
    setVisible(false);
    setSelected([term_id]); // state도 업데이트

    // 다른 데이터들 업데이트
    getBrandInfo(term_id)
      .then(res => {
        setBuilding(res.data.building);
        setFloor(res.data.floor);
        setRoomNumber(res.data.room_number);
      })
      .finally(() => {
        setGlobalLoading(false);
      });
  };

  return (
    <Controller
      name={brand_form_item_name}
      control={form.control}
      render={({ field: { onChange, value, name }, fieldState: { invalid, error } }) => (
        <CustomFormItem label="브랜드" required showError={invalid} errorMessage={error?.message}>
          <div>
            <Space align="center">
              <Button
                onClick={() => {
                  setVisible(true);
                }}
              >
                선택
              </Button>
              <div className="flex gap-x-1">
                {selected.map(term_id => (
                  <Tag key={term_id} color="primary" fill="outline" className="text-[14px]">
                    {brandMap[term_id]?.name}
                  </Tag>
                ))}
              </div>
            </Space>
            <Popup
              visible={visible}
              onMaskClick={() => {
                setVisible(false);
              }}
              destroyOnClose
            >
              <div className="p-[12px]">
                <SearchBar
                  placeholder="브랜드 검색"
                  value={searchText}
                  onChange={v => {
                    setSearchText(v);
                  }}
                />
              </div>
              <div>
                <CheckList defaultValue={[...selected]} onChange={handleBrandChange}>
                  {filteredItems.map(({ term_id, name }) => (
                    <CheckList.Item key={term_id} value={`${term_id}`}>
                      {name}
                    </CheckList.Item>
                  ))}
                </CheckList>
              </div>
            </Popup>
          </div>
        </CustomFormItem>
      )}
      rules={{ required: '브랜드를 입력해주세요!' }}
    />
  );
};

export default BrandFormItem;
