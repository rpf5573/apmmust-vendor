import { useGetMyAccount } from '@api/api-my-account';
import { Form, Input, List } from 'antd-mobile';
import { useEffect } from 'react';
import { Controller, useFormContext } from 'react-hook-form';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

export const email_form_item_name = 'email';

type EmailFormItemProps = {
  value: string;
};

const EmailFormItem: React.FC<EmailFormItemProps> = ({ value: email }) => {
  const form = useFormContext();

  return (
    <Controller
      name={email_form_item_name}
      control={form.control}
      defaultValue={email}
      render={({ field, fieldState: { invalid, error } }) => {
        return (
          <CustomFormItem label="이메일" showError={invalid} errorMessage={error?.message}>
            <Input defaultValue={email} {...field} disabled />
          </CustomFormItem>
        );
      }}
    />
  );
};

export default EmailFormItem;
