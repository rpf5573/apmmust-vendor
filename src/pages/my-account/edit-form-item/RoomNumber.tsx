import useGetBrandInfo from '@api/api-brand-info';
import { useGetMyAccount } from '@api/api-my-account';
import { Form, Input, List } from 'antd-mobile';
import { useAtomValue } from 'jotai';
import { useEffect } from 'react';
import { Controller, useFormContext } from 'react-hook-form';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

import { atom_roomNumber } from '../EditMyAccount';

const room_number_form_item_name = 'room_number';
type FloorFormItemProps = {
  value: string;
};

const RoomNumberFormItem: React.FC<FloorFormItemProps> = ({ value }) => {
  const form = useFormContext();
  const roomNumber = useAtomValue(atom_roomNumber) ?? value;

  return (
    <Controller
      name={room_number_form_item_name}
      control={form.control}
      defaultValue={roomNumber}
      render={({ field: { onChange, value, name }, fieldState: { invalid, error } }) => (
        <CustomFormItem label="호수" showError={invalid} errorMessage={error?.message}>
          <span className="adm-input-disabled">{roomNumber}</span>
        </CustomFormItem>
      )}
    />
  );
};

export default RoomNumberFormItem;
