import { Button, CheckList, Form, Input, List, Popup, SearchBar, Space, Tag } from 'antd-mobile';
import { atom, useAtom, useSetAtom } from 'jotai';
import { useMemo, useState } from 'react';

const SeasonFormItem = () => {
  const seasons = ['봄', '여름', '가을', '겨울'];
  const [visible, setVisible] = useState(false);
  const [selected, setSelected] = useState<Array<string>>([]);
  const [searchText, setSearchText] = useState('');
  const filteredItems = useMemo(() => {
    return searchText ? seasons.filter(item => item.includes(searchText)) : seasons;
  }, [seasons, searchText]);

  return (
    <Form.Item name="season" label="시즌">
      <div>
        <Space align="center">
          <Button
            onClick={() => {
              setVisible(true);
            }}
          >
            선택
          </Button>
          <div className="flex gap-x-1">
            {selected.map(size => (
              <Tag key={size} color="primary" fill="outline" className="text-[14px]">
                {size}
              </Tag>
            ))}
          </div>
        </Space>
        <Popup
          visible={visible}
          onMaskClick={() => {
            setVisible(false);
          }}
          destroyOnClose
        >
          <div className="p-[12px]">
            <SearchBar
              placeholder="시즌 검색"
              value={searchText}
              onChange={v => {
                setSearchText(v);
              }}
            />
          </div>
          <div>
            <CheckList
              defaultValue={[...selected]}
              onChange={val => {
                setSelected(val);
                setVisible(false);
              }}
            >
              {filteredItems.map(item => (
                <CheckList.Item key={item} value={item}>
                  {item}
                </CheckList.Item>
              ))}
            </CheckList>
          </div>
        </Popup>
      </div>
    </Form.Item>
  );
};

export default SeasonFormItem;
