import { useGetMyAccount } from '@api/api-my-account';
import { Form, Input, List } from 'antd-mobile';
import { Controller, useFormContext } from 'react-hook-form';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

export const bank_form_item_name = 'bank';

type BankFormItemProps = {
  value: string;
};

const BankFormItem: React.FC<BankFormItemProps> = ({ value: bank }) => {
  const form = useFormContext();

  return (
    <Controller
      name={bank_form_item_name}
      control={form.control}
      defaultValue={bank}
      render={({ field: { onChange, value, name }, fieldState: { invalid, error } }) => (
        <CustomFormItem label="은행" showError={invalid} errorMessage={error?.message}>
          <Input value={value} onChange={onChange} />
        </CustomFormItem>
      )}
    />
  );
};

export default BankFormItem;
