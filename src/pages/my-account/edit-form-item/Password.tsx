import { Form, Input, List } from 'antd-mobile';
import { Controller, useFormContext } from 'react-hook-form';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

export const password_1_form_item_name = 'password_1';
export const password_2_form_item_name = 'password_2';

const PasswordFormItem = () => {
  const form = useFormContext();

  return (
    <>
      <Controller
        name={password_1_form_item_name}
        control={form.control}
        render={({ field, fieldState: { invalid, error } }) => {
          return (
            <CustomFormItem label="비밀번호 재설정" showError={invalid} errorMessage={error?.message}>
              <Input defaultValue={'******'} />
            </CustomFormItem>
          );
        }}
      />
      <Controller
        name={password_2_form_item_name}
        control={form.control}
        render={({ field, fieldState: { invalid, error } }) => {
          return (
            <CustomFormItem label="비밀번호 한번 더입력" showError={invalid} errorMessage={error?.message}>
              <Input defaultValue={'******'} />
            </CustomFormItem>
          );
        }}
      />
    </>
  );
};

export default PasswordFormItem;
