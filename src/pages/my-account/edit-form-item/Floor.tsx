import useGetBrandInfo from '@api/api-brand-info';
import { useGetMyAccount } from '@api/api-my-account';
import { useAtomValue } from 'jotai';
import { useEffect } from 'react';
import { Controller, useFormContext } from 'react-hook-form';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

import { atom_floor } from '../EditMyAccount';

export const floor_form_item_name = 'floor';

type FloorFormItemProps = {
  value: string;
};

const FloorFormItem: React.FC<FloorFormItemProps> = ({ value }) => {
  const form = useFormContext();
  const floor = useAtomValue(atom_floor) ?? value;

  return (
    <Controller
      name={floor_form_item_name}
      control={form.control}
      defaultValue={floor}
      render={({ field: { onChange, value, name }, fieldState: { invalid, error } }) => (
        <CustomFormItem label="층" showError={invalid} errorMessage={error?.message}>
          <span className="adm-input-disabled">{floor}</span>
        </CustomFormItem>
      )}
    />
  );
};

export default FloorFormItem;
