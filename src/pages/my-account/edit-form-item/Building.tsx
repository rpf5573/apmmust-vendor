import useGetBrandInfo from '@api/api-brand-info';
import { useGetMyAccount } from '@api/api-my-account';
import { Form, Input, List } from 'antd-mobile';
import { useAtomValue, useSetAtom } from 'jotai';
import { useEffect } from 'react';
import { Controller, useFormContext } from 'react-hook-form';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

import { atom_building } from '../EditMyAccount';

type BuildingFormItemProps = {
  value: string;
};

export const building_form_item_name = 'building';

const BuildingFormItem: React.FC<BuildingFormItemProps> = ({ value }) => {
  const form = useFormContext();
  const building = useAtomValue(atom_building) ?? value;

  return (
    <Controller
      name={building_form_item_name}
      defaultValue={building}
      control={form.control}
      render={({ field: { onChange, value, name }, fieldState: { invalid, error } }) => (
        <CustomFormItem label="건물" showError={invalid} errorMessage={error?.message}>
          <span className="adm-input-disabled">{building}</span>
        </CustomFormItem>
      )}
    />
  );
};

export default BuildingFormItem;
