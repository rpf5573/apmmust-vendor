import { useGetMyAccount } from '@api/api-my-account';
import withAuth from '@hoc/withAuth';
import useLoading from '@hooks/useLoading';
import useRefetchOnPop from '@hooks/useRefetchOnPop';
import { AppScreen } from '@stackflow/plugin-basic-ui';
import { ActivityComponentType, useActivity } from '@stackflow/react';
import { Button, Form, Input } from 'antd-mobile';
import { useEffect } from 'react';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

import { useFlow } from './stackflow';

const MyAccount: ActivityComponentType = ({ params }) => {
  const { push } = useFlow();
  const getMyAccountQuery = useGetMyAccount();
  const isLoading = useRefetchOnPop(getMyAccountQuery);
  if (isLoading) return null;

  const handleClickMoveToEditPage = () => {
    push('EditMyAccount', {});
  };

  const { email, brand_name, building, floor, room_number, deposit_account, bank, phone_number } =
    getMyAccountQuery.data!.data;

  return (
    <AppScreen appBar={{ title: '마이페이지' }}>
      <Form
        footer={
          <Button block type="button" color="primary" size="large" onClick={handleClickMoveToEditPage}>
            수정하기
          </Button>
        }
        className="pb-[50px]"
      >
        <CustomFormItem label="이메일">
          <Input defaultValue={email} readOnly />
        </CustomFormItem>
        <CustomFormItem label="브랜드">
          <Input defaultValue={brand_name} readOnly />
        </CustomFormItem>
        <CustomFormItem label="건물">
          <Input defaultValue={building} readOnly />
        </CustomFormItem>
        <CustomFormItem label="층">
          <Input defaultValue={floor} readOnly />
        </CustomFormItem>
        <CustomFormItem label="호수">
          <Input defaultValue={room_number} readOnly />
        </CustomFormItem>
        <CustomFormItem label="은행">
          <Input defaultValue={bank} readOnly />
        </CustomFormItem>
        <CustomFormItem label="계좌번호">
          <Input defaultValue={deposit_account} readOnly />
        </CustomFormItem>
        <CustomFormItem label="전화번호">
          <Input defaultValue={phone_number} readOnly />
        </CustomFormItem>
      </Form>
    </AppScreen>
  );
};

export default withAuth(MyAccount);
