import { useEditProduct } from '@api/api-edit-product';
import useGetProduct from '@api/api-get-product';
import useGetProductTerms from '@api/api-terms';
import { FABRICS } from '@constants';
import atom_imageUploadStatus from '@global-state/image-upload-status';
import useLoading from '@hooks/useLoading';
import { AppScreen } from '@stackflow/plugin-basic-ui';
import { useActivity } from '@stackflow/react';
import { useQueryClient } from '@tanstack/react-query';
import { Button, List } from 'antd-mobile';
import { useAtomValue, useSetAtom } from 'jotai';
import { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { toast } from 'react-hot-toast';
import _ from 'underscore';

import CategoryFormItem from './form-item/CategoryFormItem';
import ColourFormItem from './form-item/ColourFormItem';
import FeaturedImageFormItem from './form-item/FeaturedImageFormItem';
import GalleryImageFormItem from './form-item/GalleryImageFormItem';
import GenderFormItem from './form-item/GenderFormItem';
import MadeInFormItem from './form-item/MadeIn';
import MixingRateFormItem from './form-item/MixingRateFormItem';
import NameFormItem from './form-item/NameFormItem';
import PriceFormItem from './form-item/PriceFormItem';
import SalePriceFormItem from './form-item/SalePriceFormItem';
import SeasonFormItem from './form-item/SeasonFormItem';
import SizeFormItem from './form-item/SizeFormItem';
import WeightFormItem from './form-item/WeightFormItem';
import { useFlow } from './stackflow';

type EditProductProps = {
  params: {
    product_id: string;
  };
};

const EditProduct: React.FC<EditProductProps> = ({ params }) => {
  const { product_id } = params;

  const form = useForm();
  const imageUploadStatus = useAtomValue(atom_imageUploadStatus);
  const [isEditingProduct, setIsMakingProduct] = useState(false);
  const getProductQuery = useGetProduct(product_id);
  const getProductTermsQuery = useGetProductTerms();
  const editProduct = useEditProduct();
  const queryClient = useQueryClient();
  const activity = useActivity();
  const { pop } = useFlow();

  useLoading(getProductQuery.isLoading || getProductTermsQuery.isLoading);
  if (getProductQuery.isLoading || getProductTermsQuery.isLoading) return null;

  const product = getProductQuery.data!.data;
  const termData = getProductTermsQuery.data!.data;

  const onSubmit = (data: any) => {
    setIsMakingProduct(true);

    if (!data) return;
    if (!_.isObject(data)) return;

    FABRICS.forEach(fabric => {
      if (data[fabric.value]) {
        if (!data['mixing_rates']) {
          data['mixing_rates'] = {};
        }
        data['mixing_rates'][fabric.value] = data[fabric.value];
        delete data[fabric.value];
      }
    });

    data['product_id'] = product.id;

    editProduct
      .mutateAsync(data as any)
      .then(res => {
        toast.success(res.message);
        // 모든 쿼리를 무효화 한다
        // 아니 그냥 엑티비티가 top에 올라올때마다 항상 새로 데이터를 refetch하면 될듯
        pop();
        // navigate('/products', { replace: true });
      })
      .catch(err => {
        console.error(err);
        toast.error('상품 생성에 실패했습니다');
      })
      .finally(() => {
        setIsMakingProduct(false);
      });
  };

  return (
    <AppScreen appBar={{ title: '상품 수정' }}>
      <FormProvider {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)} className="adm-form mb-10">
          <List>
            <NameFormItem product={product} />
            <PriceFormItem product={product} />
            <SalePriceFormItem product={product} />

            <WeightFormItem product={product} />
            <SizeFormItem disabled product={product} terms={termData.sizes} termMap={termData.size_map} />
            <ColourFormItem disabled product={product} terms={termData.colours} termMap={termData.colour_map} />
            <FeaturedImageFormItem product={product} />
            <GalleryImageFormItem product={product} />
            <GenderFormItem product={product} terms={termData.genders} termMap={termData.gender_map} />
            <CategoryFormItem product={product} terms={termData.categories} termMap={termData.category_map} />
            <MixingRateFormItem product={product} />
            <SeasonFormItem product={product} terms={termData.seasons} termMap={termData.season_map} />
            <MadeInFormItem product={product} terms={termData.made_ins} termMap={termData.made_in_map} />
          </List>
          <div className="adm-form-footer">
            <Button
              block
              type="submit"
              color="primary"
              size="large"
              disabled={
                imageUploadStatus.isFeaturedImageUploading === true ||
                imageUploadStatus.isGalleryImageUploading === true ||
                Object.keys(form.formState.errors).length > 0
              }
              loading={isEditingProduct}
            >
              수정하기
            </Button>
          </div>
        </form>
      </FormProvider>
    </AppScreen>
  );
};

export default EditProduct;
