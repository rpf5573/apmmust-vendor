import { deleteProduct } from '@api/api-delete-product';
import useGetProduct from '@api/api-get-product';
import { Product, Term } from '@custom-types';
import useLoading from '@hooks/useLoading';
import { AppScreen } from '@stackflow/plugin-basic-ui';
import { useActivity } from '@stackflow/react';
import { message } from 'antd';
import { Button, Dialog, Form, Input, Modal, Swiper } from 'antd-mobile';
import { DialogShowHandler } from 'antd-mobile/es/components/dialog';
import { useEffect, useRef, useState } from 'react';
import _ from 'underscore';

import { useFlow } from './stackflow';

type ProductItemDetailProps = {
  params: {
    product_id: string;
    after_edit: boolean;
  };
};

const ProductItemDetail: React.FC<ProductItemDetailProps> = ({ params }) => {
  const { push, pop } = useFlow();
  const activity = useActivity();

  const { product_id } = params;

  const handler = useRef<DialogShowHandler>();

  const getProductQuery = useGetProduct(product_id);
  const setGlobalLoading = useLoading(getProductQuery.isLoading);
  useEffect(() => {
    setGlobalLoading(true);
    if (activity.isTop) {
      getProductQuery.refetch().finally(() => {
        setGlobalLoading(false);
      });
    }
  }, [activity.isTop]);

  // 로딩화면 보여주기
  if (getProductQuery.isLoading) return null;

  const product = getProductQuery.data!.data;

  const imageUrls = [product.featured_image_url, ...(product.gallery_image_urls ?? [])];

  const handleClickEditButton = () => {
    push('EditProduct', { product_id: product.id });
  };

  const handleClickDeleteButton = () => {
    handler.current = Dialog.show({
      title: '상품을 삭제하시겠습니까?',
      content: '삭제된 상품은 복구될 수 없습니다',
      actions: [
        [
          {
            key: 'cancel',
            text: '취소',
          },
          {
            key: 'delete',
            text: '삭제하기',
            bold: true,
            danger: true,
          },
        ],
      ],
      closeOnMaskClick: true,
      onAction: (action: any) => {
        if (action.key === 'delete') {
          handler.current?.close();
          setTimeout(() => {
            setGlobalLoading(true);
            deleteProduct(product.id)
              .then(res => {
                message.success(res.message);
                setTimeout(() => {
                  pop();
                }, 200);
              })
              .catch(err => {
                message.error(err.message);
              })
              .finally(() => {
                setGlobalLoading(false);
              });
          }, 300);
        }
        if (action.key === 'cancel') {
          handler.current?.close();
        }
      },
    });
  };

  return (
    <AppScreen appBar={{ title: '상품 상세' }}>
      <div className="product-item-detail pb-[50px]">
        <Swiper className="image-slider" autoplay={false} loop={true}>
          {imageUrls.map((url, index) => (
            <Swiper.Item key={index}>
              <img className="w-[100vw]" src={url} />
            </Swiper.Item>
          ))}
        </Swiper>
        <Form className="mb-4">
          <Form.Item label="상품명">
            <Input value={product.name} readOnly />
          </Form.Item>
          <Form.Item label="가격">
            <Input value={product.regular_price} readOnly />
          </Form.Item>
          {product.sale_price && (
            <Form.Item label="할인 가격">
              <Input value={product.sale_price} readOnly />
            </Form.Item>
          )}
          {product.weight && (
            <Form.Item label="무게">
              <Input value={product.weight} readOnly />
            </Form.Item>
          )}
          {product.sizes && _.isArray(product.sizes) && (
            <Form.Item label="사이즈">
              <Input value={termArrayToString(product.sizes)} readOnly />
            </Form.Item>
          )}
          {product.colours && _.isArray(product.colours) && (
            <Form.Item label="색상">
              <Input value={termArrayToString(product.colours)} readOnly />
            </Form.Item>
          )}
          {_.isArray(product.genders) && product.genders.length > 0 && (
            <Form.Item label="성별">
              <Input value={product.genders[0].name} readOnly />
            </Form.Item>
          )}
          {product.categories && _.isArray(product.categories) && (
            <Form.Item label="카테고리">
              <Input value={termArrayToString(product.categories)} readOnly />
            </Form.Item>
          )}
          {product.mixing_rates && _.isArray(product.mixing_rates) && (
            <Form.Item label="혼용률">
              <Input value={mixingRatesToString(product.mixing_rates)} readOnly />
            </Form.Item>
          )}
          {product.seasons && _.isArray(product.seasons) && (
            <Form.Item label="시즌">
              <Input value={termArrayToString(product.seasons)} readOnly />
            </Form.Item>
          )}
          {product.made_ins && _.isArray(product.made_ins) && (
            <Form.Item label="제조국">
              <Input value={termArrayToString(product.made_ins)} readOnly />
            </Form.Item>
          )}
          <Form.Item label="SKU">
            <Input value={product.sku} readOnly />
          </Form.Item>
        </Form>

        <div className="product-actions flex gap-3 px-3 py-3">
          <Button block color="danger" size="large" onClick={handleClickDeleteButton}>
            삭제하기
          </Button>
          <Button block color="primary" size="large" onClick={handleClickEditButton}>
            수정하기
          </Button>
        </div>
      </div>
    </AppScreen>
  );
};

export default ProductItemDetail;

const termArrayToString = (terms: Array<Term>) => {
  return terms
    .reduce((acc, cur) => {
      return `${acc}, ${cur.name}`;
    }, '')
    .substring(1);
};

const mixingRatesToString = (mixingRates: Required<Product>['mixing_rates']) => {
  return mixingRates
    .reduce((acc, cur) => {
      return `${acc}, ${cur.label} : ${cur.value}`;
    }, '')
    .substring(1);
};
