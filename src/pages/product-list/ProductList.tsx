import { useGetProducts } from '@api/api-get-product';
import useLoading from '@hooks/useLoading';
import { AppScreen } from '@stackflow/plugin-basic-ui';
import '@stackflow/plugin-basic-ui/index.css';
import { ActivityComponentType, useActivity } from '@stackflow/react';
import { Pagination as AntPagination } from 'antd';
import { List } from 'antd-mobile';
import { useEffect, useState } from 'react';

import ProductListItem from './ProductListItem';

const ProductList: ActivityComponentType = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const postsPerPage = 8;
  const getProductsQuery = useGetProducts({ page: currentPage, postsPerPage });
  const activity = useActivity();

  const setGlobalLoading = useLoading(getProductsQuery.isLoading); // global loading창 보여주고 여기는 그리지 않는다

  useEffect(() => {
    setGlobalLoading(true);
    if (activity.isTop) {
      getProductsQuery.refetch().finally(() => {
        setGlobalLoading(false);
      });
    }
  }, [activity.isTop]);

  if (getProductsQuery.isLoading) {
    return null;
  }
  const products = getProductsQuery.data!.data ?? [];
  console.log('getProductsQuery.data : ', getProductsQuery.data);

  return (
    <AppScreen appBar={{ title: '상품 목록' }}>
      <div className="h-full pb-[70px] flex flex-col">
        <List className="mb-3 flex-1">
          {products.map(product => (
            <ProductListItem key={product.sku} product={product} />
          ))}
        </List>
        <div className="flex justify-center">
          <AntPagination
            current={currentPage}
            defaultCurrent={1}
            total={getProductsQuery.data?.total_posts ?? 10}
            pageSize={postsPerPage}
            onChange={page => setCurrentPage(page)}
            showSizeChanger={false}
            disabled={getProductsQuery.isFetching}
          />
        </div>
      </div>
    </AppScreen>
  );
};

export default ProductList;
