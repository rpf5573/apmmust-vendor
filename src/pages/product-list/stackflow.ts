import { basicUIPlugin } from '@stackflow/plugin-basic-ui';
import { basicRendererPlugin } from '@stackflow/plugin-renderer-basic';
import { stackflow } from '@stackflow/react';

import EditProduct from './EditProduct';
import ProductItemDetail from './ProductItemDetail';
import ProductList from './ProductList';

export const { Stack, useFlow } = stackflow({
  transitionDuration: 350,
  activities: {
    ProductList,
    ProductItemDetail,
    EditProduct,
  },
  plugins: [
    basicRendererPlugin(),
    basicUIPlugin({
      theme: 'cupertino',
    }),
  ],
  initialActivity: () => 'ProductList',
});
