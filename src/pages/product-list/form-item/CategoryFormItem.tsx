import useGetProductTerms, { ApiTerms } from '@api/api-terms';
import { Product, Term, TreeTerm } from '@custom-types';
import { filterCategoryByGender } from '@utils';
import { Button, CheckList, Form, Popup, SearchBar, Space, Tag } from 'antd-mobile';
import { useAtomValue } from 'jotai';
import React, { useEffect, useMemo, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { toast } from 'react-hot-toast';
import { useUpdateEffect } from 'react-use';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

import { atom_gender, atom_is_gender_click } from './GenderFormItem';

export const category_form_item_name = 'categories';

type CategoryFormItemProps = {
  product: Product;
  terms: ApiTerms['get']['responseData']['data']['categories'];
  termMap: ApiTerms['get']['responseData']['data']['category_map'];
};

const CategoryFormItem: React.FC<CategoryFormItemProps> = ({ product, terms: categories, termMap: categoryMap }) => {
  const form = useFormContext();
  const [visible, setVisible] = useState(false);
  const gender = useAtomValue(atom_gender);
  const isGenderClick = useAtomValue(atom_is_gender_click);

  console.log('gender : ', gender);

  const genderBasedCategories = filterCategoryByGender(categories, gender ?? 'c-unisex');

  const [selected, setSelected] = useState<string[]>((product.categories ?? []).map(item => `${item.term_id}`));

  // selected가 바뀌면 form에도 업데이트
  useEffect(() => {
    form.setValue(category_form_item_name, selected);
    form.clearErrors(category_form_item_name);
  }, [selected]);

  // gender를 '클릭하면'(단순히 바뀌면이 아니다) category를 초기화한다.
  useUpdateEffect(() => {
    gender !== null && isGenderClick && setSelected([]);
  }, [gender]);

  const handleCategoryChange = (val: Array<string>) => {
    setVisible(false);
    setSelected(val);
  };

  const handleClickCategorySelectViewOpenButton = () => {
    if (!gender) {
      toast.error('성별을 먼저 선택해주세요!');
      return;
    }
    setVisible(true);
  };

  const renderItem = (item: TreeTerm) => {
    return (
      <React.Fragment key={item.term_id}>
        {/* 자식 요소가 있으면 선택 못한다! */}
        <CheckList.Item disabled={(item.children ?? []).length > 0} value={`${item.term_id}`}>
          {item.name}
        </CheckList.Item>
        {item.children && item.children.length > 0 && (
          <div style={{ marginLeft: '20px' }}>{item.children.map(renderItem)}</div>
        )}
      </React.Fragment>
    );
  };

  return (
    <Controller
      name={category_form_item_name}
      control={form.control}
      render={({ field, fieldState: { invalid, error } }) => {
        return (
          <CustomFormItem label="카테고리" required showError={invalid} errorMessage={error?.message}>
            <Space align="center">
              <Button onClick={handleClickCategorySelectViewOpenButton}>선택</Button>
              <div className="flex gap-x-1">
                {selected.map(term_id => (
                  <Tag key={term_id} color="primary" fill="outline" className="text-[14px]">
                    {categoryMap[term_id]?.name}
                  </Tag>
                ))}
              </div>
            </Space>
            <Popup
              className="form-item-content--category__popup"
              visible={visible}
              onMaskClick={() => {
                setVisible(false);
              }}
              destroyOnClose
            >
              <div className="check-list-container">
                <CheckList multiple defaultValue={[...selected]} onChange={handleCategoryChange}>
                  {genderBasedCategories.map(renderItem)}
                </CheckList>
              </div>
            </Popup>
          </CustomFormItem>
        );
      }}
      rules={{ required: '카테고리를 입력해주세요' }}
    />
  );
};

export default CategoryFormItem;
