import { Product } from '@custom-types';
import { Form, Input } from 'antd-mobile';
import { useEffect } from 'react';
import { Controller, useFormContext } from 'react-hook-form';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

type PriceFormItemProps = {
  product: Product;
};

const price_form_item_name = 'regular_price';

const PriceFormItem: React.FC<PriceFormItemProps> = ({ product }) => {
  const form = useFormContext();

  useEffect(() => {
    form.setValue(price_form_item_name, product.regular_price);
  }, [product]);

  return (
    <Controller
      name={price_form_item_name}
      control={form.control}
      render={({ field, fieldState: { invalid, error } }) => (
        <CustomFormItem label="가격" required showError={invalid} errorMessage={error?.message}>
          <Input type="number" placeholder="상품 가격 입력" {...field} />
        </CustomFormItem>
      )}
      rules={{ required: '가격을 입력해주세요' }}
    />
  );
};

export default PriceFormItem;
