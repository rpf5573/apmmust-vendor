import useGetProductTerms, { ApiTerms } from '@api/api-terms';
import { Product } from '@custom-types';
import { Button, CheckList, Form, Input, Popup, SearchBar, Space, Tag } from 'antd-mobile';
import { FormInstance } from 'rc-field-form';
import { useEffect, useMemo, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

export const size_form_item_name = 'sizes';

type SizeFormItemProps = {
  product: Product;
  terms: ApiTerms['get']['responseData']['data']['sizes'];
  termMap: ApiTerms['get']['responseData']['data']['size_map'];
  disabled: boolean;
};

const SizeFormItem: React.FC<SizeFormItemProps> = ({ product, terms: sizes, termMap: sizeMap, disabled }) => {
  const form = useFormContext();

  const [selected, setSelected] = useState<string[]>((product.sizes ?? []).map(item => `${item.term_id}`));
  const [visible, setVisible] = useState(false);
  const [searchText, setSearchText] = useState('');
  const getProductTermsQuery = useGetProductTerms();

  const sizeNamesForSearch = useMemo(() => {
    return sizes.map(item => item.name.toLocaleLowerCase().replace(/\s+/g, ''));
  }, [sizes]);

  const filteredItems = useMemo(() => {
    return searchText
      ? sizes.filter((_, index) =>
          sizeNamesForSearch[index].includes(searchText.toLocaleLowerCase().replace(/\s+/g, '')),
        )
      : sizes;
  }, [sizes, searchText]);

  // selected가 바뀌면 form에도 업데이트
  useEffect(() => {
    form.setValue(size_form_item_name, selected);
  }, [selected]);

  const handleSizeChange = (val: Array<string>) => {
    setVisible(false);
    setSelected(val);
  };

  return (
    <Controller
      name={size_form_item_name}
      control={form.control}
      render={({ field: { name }, fieldState: { invalid, error } }) => (
        <CustomFormItem label="사이즈" required showError={invalid} errorMessage={error?.message}>
          <Space align="center">
            <Button
              disabled={disabled}
              onClick={() => {
                setVisible(true);
              }}
            >
              선택
            </Button>
            <div className="flex gap-x-1">
              {selected.map(term_id => (
                <Tag key={term_id} color="primary" fill="outline" className="text-[14px]">
                  {sizeMap[term_id]?.name}
                </Tag>
              ))}
            </div>
          </Space>
          <Popup
            className="form-item-content--size__popup"
            visible={visible}
            onMaskClick={() => {
              setVisible(false);
            }}
            destroyOnClose
          >
            <div className="search-bar-container p-[12px]">
              <SearchBar
                placeholder="사이즈 검색"
                value={searchText}
                onChange={v => {
                  setSearchText(v);
                }}
              />
            </div>
            <div className="check-list-container">
              <CheckList multiple defaultValue={[...selected]} onChange={handleSizeChange}>
                {filteredItems.map(item => (
                  <CheckList.Item key={item.term_id} value={`${item.term_id}`}>
                    {item.name}
                  </CheckList.Item>
                ))}
              </CheckList>
            </div>
          </Popup>
        </CustomFormItem>
      )}
      rules={{ required: '사이즈를 입력해주세요' }}
    />
  );
};

export default SizeFormItem;
