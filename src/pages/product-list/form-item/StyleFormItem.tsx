import { Product } from '@custom-types';
import { Button, CheckList, Form, Popup, SearchBar, Space, Tag } from 'antd-mobile';
import { useMemo, useState } from 'react';

type StyleFormItemProps = {
  product: Product;
};

const StyleFormItem: React.FC<StyleFormItemProps> = ({ product }) => {
  const sizeItems = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];
  const [visible, setVisible] = useState(false);
  const [selected, setSelected] = useState<Array<string>>([]);
  const [searchText, setSearchText] = useState('');
  const filteredItems = useMemo(() => {
    if (searchText) {
      return sizeItems.filter(item => item.includes(searchText));
    } else {
      return sizeItems;
    }
  }, [sizeItems, searchText]);

  return (
    <Form.Item label="스타일">
      <div>
        <Space align="center">
          <Button
            onClick={() => {
              setVisible(true);
            }}
          >
            선택
          </Button>
          <div className="flex gap-x-1">
            {selected.map(size => (
              <Tag key={size} color="primary" fill="outline" className="text-[14px]">
                {size}
              </Tag>
            ))}
          </div>
        </Space>
        <Popup
          className="form-item-content--size__popup"
          visible={visible}
          onMaskClick={() => {
            setVisible(false);
          }}
          destroyOnClose
        >
          <div className="search-bar-container p-[12px]">
            <SearchBar
              placeholder="스타일 검색"
              value={searchText}
              onChange={v => {
                setSearchText(v);
              }}
            />
          </div>
          <div className="check-list-container">
            <CheckList
              multiple
              defaultValue={[...selected]}
              onChange={val => {
                setSelected(val);
              }}
            >
              {filteredItems.map(item => (
                <CheckList.Item key={item} value={item}>
                  {item}
                </CheckList.Item>
              ))}
            </CheckList>
          </div>
        </Popup>
      </div>
    </Form.Item>
  );
};

export default StyleFormItem;
