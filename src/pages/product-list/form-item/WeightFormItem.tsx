import { Product } from '@custom-types';
import { Button, CheckList, Form, Input, Popup, SearchBar, Space, Tag } from 'antd-mobile';
import { useEffect } from 'react';
import { Controller, useFormContext } from 'react-hook-form';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

type WeightFormItemProps = {
  product: Product;
};

const weight_form_item_name = 'weight';

const WeightFormItem: React.FC<WeightFormItemProps> = ({ product }) => {
  const form = useFormContext();

  useEffect(() => {
    form.setValue(weight_form_item_name, product.weight);
  }, [product]);

  return (
    <Controller
      name="weight"
      control={form.control}
      render={({ field, fieldState: { invalid, error } }) => (
        <CustomFormItem label="상품 무게" showError={invalid} errorMessage={error?.message}>
          <Input placeholder="무게 입력" {...field} />
        </CustomFormItem>
      )}
    />
  );
};

export default WeightFormItem;
