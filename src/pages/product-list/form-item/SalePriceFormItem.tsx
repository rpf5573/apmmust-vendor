import { Product } from '@custom-types';
import { Form, Input } from 'antd-mobile';
import { useEffect } from 'react';
import { Controller, useFormContext } from 'react-hook-form';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

type SalePriceFormItemProps = {
  product: Product;
};

const sale_price_form_item_name = 'sale_price';

const SalePriceFormItem: React.FC<SalePriceFormItemProps> = ({ product }) => {
  const form = useFormContext();

  useEffect(() => {
    form.setValue(sale_price_form_item_name, product.sale_price);
  }, [product]);

  return (
    <Controller
      name={sale_price_form_item_name}
      control={form.control}
      render={({ field, fieldState: { invalid, error } }) => (
        <CustomFormItem label="할인 가격" showError={invalid} errorMessage={error?.message}>
          <Input type="number" placeholder="할인된 상품 가격 입력" {...field} />
        </CustomFormItem>
      )}
    />
  );
};

export default SalePriceFormItem;
