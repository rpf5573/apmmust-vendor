import { Product } from '@custom-types';
import { Form, Input } from 'antd-mobile';
import { useEffect } from 'react';
import { Controller, useFormContext } from 'react-hook-form';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

const name_form_item = 'name';

type NameFormItemProps = {
  product: Product;
};

const NameFormItem: React.FC<NameFormItemProps> = ({ product }) => {
  const form = useFormContext();

  useEffect(() => {
    form.setValue(name_form_item, product.name);
  }, [product]);

  return (
    <Controller
      name={name_form_item}
      control={form.control}
      render={({ field: { ref, ...field }, fieldState: { invalid, error } }) => {
        return (
          <CustomFormItem label="상품명" required showError={invalid} errorMessage={error?.message}>
            <Input placeholder="상품 이름 입력" {...field} ref={ref} />
          </CustomFormItem>
        );
      }}
      rules={{ required: '상품명을 입력해주세요' }}
    />
  );
};

export default NameFormItem;
