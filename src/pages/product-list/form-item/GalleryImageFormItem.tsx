import { apmmustS3Client, bucket, getImageUrl } from '@api/api-s3';
import { PutObjectCommand } from '@aws-sdk/client-s3';
import { Product } from '@custom-types';
import atom_imageUploadStatus from '@global-state/image-upload-status';
import { generateProductFilename } from '@utils';
import { Dialog, Form, ImageUploader, Space, Swiper, Tabs, Toast } from 'antd-mobile';
import type { ImageUploadItem } from 'antd-mobile';
import { UploadTask } from 'antd-mobile/es/components/image-uploader';
import { useSetAtom } from 'jotai';
import { FormInstance } from 'rc-field-form';
import { useEffect, useRef, useState } from 'react';
import { Controller, useForm, useFormContext } from 'react-hook-form';
import { toast } from 'react-hot-toast';
import _ from 'underscore';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

const maxCount = 14;

const gallery_image_form_item_name = 'gallery_image_urls';

type GalleryImageFormItemProps = {
  product: Product;
};

const GalleryImageFormItem: React.FC<GalleryImageFormItemProps> = ({ product }) => {
  const form = useFormContext();
  const [selected, setSelected] = useState<Array<ImageUploadItem>>(
    (product.gallery_image_urls ?? []).map(url => ({ url })),
  );

  const setImageUploadStatus = useSetAtom(atom_imageUploadStatus);

  useEffect(() => {
    form.setValue(
      gallery_image_form_item_name,
      selected.map(item => item.url),
    );
    form.clearErrors(gallery_image_form_item_name);
  }, [selected]);

  async function handleUpload(file: any) {
    setImageUploadStatus({ isGalleryImageUploading: true });

    const key = generateProductFilename(file, 'vendor');

    const params = {
      Bucket: bucket, // The path to the directory you want to upload the object to, starting with your Space name.
      Key: key, // Object key, referenced whenever you want to access this file later.
      Body: file, // The object's contents. This variable is an object, not a string.
      ACL: 'public-read', // Defines ACL permissions, such as private or public.
      Metadata: {},
    };

    try {
      const data = await apmmustS3Client.send(new PutObjectCommand(params));
      setImageUploadStatus({ isGalleryImageUploading: false }); // 이미지 업로드에 성공한 경우에만 등록하기 가능함

      const url = getImageUrl(params.Key);
      return { url };
    } catch (err) {
      toast.error('이미지 업로드 실패');
      throw new Error('이미지 업로드 실패');
    }
  }

  // 업로드 실패한 이미지를 삭제할때 '등록하기' 버튼도 다시 활성화 되도록 한다
  const handleUploadQueueChange = (tasks: UploadTask[]) => {
    if (tasks.filter(task => task.status === 'pending').length > 0) return;
    if (tasks.filter(task => task.status === 'fail').length > 0) return;

    setImageUploadStatus({ isGalleryImageUploading: false });
  };

  return (
    <Controller
      name={gallery_image_form_item_name}
      control={form.control}
      render={({ field: { onChange, value, name }, fieldState: { invalid, error } }) => (
        <CustomFormItem label="갤러리 이미지" required showError={invalid} errorMessage={error?.message}>
          <ImageUploader
            defaultValue={selected}
            value={selected}
            maxCount={maxCount}
            onChange={items => {
              setSelected(items);
            }}
            upload={handleUpload}
            onUploadQueueChange={handleUploadQueueChange}
            columns={4}
          />
        </CustomFormItem>
      )}
      rules={{ required: '갤러리 이미지를 선택해주세요' }}
    />
  );
};

export default GalleryImageFormItem;
