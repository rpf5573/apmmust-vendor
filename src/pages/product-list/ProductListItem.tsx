import { Product } from '@custom-types';
import { Avatar, ImageViewer, List } from 'antd-mobile';
import { atom, useAtom } from 'jotai';
import { useEffect, useState } from 'react';

import { useFlow } from './stackflow';

type ProductListItemProps = {
  product: Product;
};

const ProductListItem: React.FC<ProductListItemProps> = ({ product }) => {
  const { push } = useFlow();
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    console.log('ProductListItem is loaded');
  }, []);

  const categoryNames = product.categories?.map(category => category.name).join(', ');

  const description = `일반가격: ${product.regular_price} / 할인가격: ${product.sale_price} / 카테고리: ${categoryNames}`;

  const handleListItemClick = () => {
    push('ProductItemDetail', { product_id: product.id });
  };

  return (
    <List.Item
      prefix={<Avatar src={product.featured_image_url} />}
      description={description}
      onClick={handleListItemClick}
    >
      {product.name}
      <ImageViewer
        image={product.featured_image_url}
        visible={visible}
        onClose={() => {
          setVisible(false);
        }}
      />
    </List.Item>
  );
};

export default ProductListItem;
