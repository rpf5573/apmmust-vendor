import { useGetNotices } from '@api/api-get-notice';
import useLoading from '@hooks/useLoading';
import { AppScreen } from '@stackflow/plugin-basic-ui';
import '@stackflow/plugin-basic-ui/index.css';
import { ActivityComponentType, useActivity } from '@stackflow/react';
import { Pagination as AntPagination } from 'antd';
import { List } from 'antd-mobile';
import { useEffect, useState } from 'react';

import NoticeListItem from './NoticeListItem';

const NoticeList: ActivityComponentType = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const postsPerPage = 8;
  const getNoticesQuery = useGetNotices({ page: currentPage, postsPerPage });
  const activity = useActivity();

  const setGlobalLoading = useLoading(getNoticesQuery.isLoading); // global loading창 보여주고 여기는 그리지 않는다

  useEffect(() => {
    setGlobalLoading(true);
    if (activity.isTop) {
      getNoticesQuery.refetch().finally(() => {
        setGlobalLoading(false);
      });
    }
  }, [activity.isTop]);

  if (getNoticesQuery.isLoading) {
    return null;
  }
  const notices = getNoticesQuery.data!.data ?? [];
  console.log('getNoticesQuery.data : ', getNoticesQuery.data);

  return (
    <AppScreen appBar={{ title: '공지사항 목록' }}>
      <div className="h-full pb-[70px] flex flex-col">
        <List className="mb-3 flex-1">
          {notices.map(notice => (
            <NoticeListItem key={notice.id} notice={notice} />
          ))}
        </List>
        <div className="flex justify-center">
          <AntPagination
            current={currentPage}
            defaultCurrent={1}
            total={getNoticesQuery.data?.total_posts ?? 10}
            pageSize={postsPerPage}
            onChange={page => setCurrentPage(page)}
            showSizeChanger={false}
            disabled={getNoticesQuery.isFetching}
          />
        </div>
      </div>
    </AppScreen>
  );
};

export default NoticeList;
