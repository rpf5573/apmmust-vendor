import { basicUIPlugin } from '@stackflow/plugin-basic-ui';
import { basicRendererPlugin } from '@stackflow/plugin-renderer-basic';
import { stackflow } from '@stackflow/react';

import NoticeItemDetail from './NoticeItemDetail';
import NoticeList from './NoticeList';

export const { Stack, useFlow } = stackflow({
  transitionDuration: 350,
  activities: {
    NoticeList,
    NoticeItemDetail,
  },
  plugins: [
    basicRendererPlugin(),
    basicUIPlugin({
      theme: 'cupertino',
    }),
  ],
  initialActivity: () => 'NoticeList',
});
