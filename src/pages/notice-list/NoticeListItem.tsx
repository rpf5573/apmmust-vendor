import { Notice } from '@custom-types';
import { List } from 'antd-mobile';
import { useState } from 'react';

import { useFlow } from './stackflow';

type NoticeListItemProps = {
  notice: Notice;
};

const NoticeListItem: React.FC<NoticeListItemProps> = ({ notice }) => {
  const { push } = useFlow();
  const [visible, setVisible] = useState(false);

  const { id, title, content, created_at } = notice;

  const handleListItemClick = () => {
    push('NoticeItemDetail', { noticeId: notice.id });
  };

  return <List.Item title={title} onClick={handleListItemClick} description={notice.created_at}></List.Item>;
};

export default NoticeListItem;
