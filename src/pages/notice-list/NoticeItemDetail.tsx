import useGetNotice from '@api/api-get-notice';
import useLoading from '@hooks/useLoading';
import { AppScreen } from '@stackflow/plugin-basic-ui';
import { useActivity } from '@stackflow/react';
import { DialogShowHandler } from 'antd-mobile/es/components/dialog';
import { useEffect, useRef, useState } from 'react';
import _ from 'underscore';

import { useFlow } from './stackflow';

type NoticeItemDetailProps = {
  params: {
    noticeId: string;
  };
};

const NoticeItemDetail: React.FC<NoticeItemDetailProps> = ({ params: { noticeId } }) => {
  const { push, pop } = useFlow();
  const activity = useActivity();

  const handler = useRef<DialogShowHandler>();

  const getNoticeQuery = useGetNotice(noticeId);
  const setGlobalLoading = useLoading(getNoticeQuery.isLoading);
  useEffect(() => {
    setGlobalLoading(true);
    if (activity.isTop) {
      getNoticeQuery.refetch().finally(() => {
        setGlobalLoading(false);
      });
    }
  }, [activity.isTop]);

  // 로딩화면 보여주기
  if (getNoticeQuery.isLoading) return null;

  const notice = getNoticeQuery.data!.data;

  return (
    <AppScreen appBar={{ title: '공지사항' }}>
      <div className="notice-item-detail pb-[50px]">
        <header>
          <h1 className="title">{notice.title}</h1>
          <p className="date">{notice.created_at}</p>
        </header>
        <div className="content" dangerouslySetInnerHTML={{ __html: notice.content }}></div>
      </div>
    </AppScreen>
  );
};

export default NoticeItemDetail;
