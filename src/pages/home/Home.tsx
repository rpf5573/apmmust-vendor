import AddProduct from '@add-product-page/AddProduct';
import AddProductStackFlowContainer from '@add-product-page/AddProductStackFlowContainer';
import withAuth from '@hoc/withAuth';
import { NavBar, TabBar } from 'antd-mobile';
import { AddOutline, BellOutline, FolderOutline, UnorderedListOutline, UserOutline } from 'antd-mobile-icons';
import { atom, useAtom, useAtomValue } from 'jotai';
import React, { FC } from 'react';
import { Route, Routes, useLocation, useNavigate } from 'react-router-dom';

import MyAccountStackFlowContainer from '@pages/my-account/MyAccountStackFlowContainer';
import NoticeListStackFlowContainer from '@pages/notice-list/NoticeListStackFlowContainer';
import ProductListStackFlowContainer from '@pages/product-list/ProductListStackFlowContainer';

import GlobalLoading from '@components/@shared/GlobalLoading';

export const atom_showGlobalLoading = atom(false);

const Home = () => {
  const isGlobalLoading = useAtomValue(atom_showGlobalLoading);
  const navigate = useNavigate();
  const location = useLocation();
  const { pathname } = location;
  const handleChange = (path: string) => {
    navigate(path);
  };

  const tabs = [
    {
      key: '/products',
      title: '상품목록',
      icon: <UnorderedListOutline />,
    },
    {
      key: '/add-product',
      title: '상품등록',
      icon: <AddOutline />,
    },
    {
      key: '/orders',
      title: '주문목록',
      icon: <FolderOutline />,
    },
    {
      key: '/notices',
      title: '공지사항',
      icon: <BellOutline />,
    },
    {
      key: '/my-account',
      title: '마이페이지',
      icon: <UserOutline />,
    },
  ];

  return (
    <>
      <GlobalLoading visible={isGlobalLoading} />
      <div className="flex flex-col min-h-[100vh]">
        <div className="flex-1 pb-[50px]">
          <Routes>
            <Route path="products" element={<ProductListStackFlowContainer />} />
            <Route path="product-detail/:product_id" element={<ProductListStackFlowContainer />} />
            <Route path="edit-product/:product_id" element={<ProductListStackFlowContainer />} />
            <Route path="add-product" element={<AddProductStackFlowContainer />} />
            <Route path="orders" element={<div></div>} />
            <Route path="notices" element={<NoticeListStackFlowContainer />} />
            <Route path="my-account" element={<MyAccountStackFlowContainer />} />
            <Route path="*" element={<ProductListStackFlowContainer />} />
          </Routes>
        </div>
        <div className="fixed left-0 right-0 bottom-0 z-10 bg-white shadow-[0px_0px_20px_-15px_black]">
          <TabBar activeKey={pathname} onChange={handleChange}>
            {tabs.map(item => (
              <TabBar.Item key={item.key} icon={item.icon} title={item.title} />
            ))}
          </TabBar>
        </div>
      </div>
    </>
  );
};

export default withAuth(Home);
