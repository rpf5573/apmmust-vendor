import usePostSignup from '@api/api-sign-up';
import useGetProductTerms from '@api/api-terms';
import AuthController from '@auth/AuthController';
import { Button, Form, Input } from 'antd-mobile';
import { useForm } from 'rc-field-form';
import { useEffect, useLayoutEffect, useState } from 'react';
import { toast } from 'react-hot-toast';
import { useNavigate } from 'react-router-dom';
import _ from 'underscore';

import BrandFormItem from '@components/brand-form-item/BrandFormItem';

const SignUp = () => {
  const [form] = useForm();
  const postSignupMutation = usePostSignup();
  const [isSignUpLoading, setIsSignUpLoading] = useState(false);
  const getTermsQuery = useGetProductTerms();

  const navigate = useNavigate();

  // 로그인한 상태라면 홈으로 되돌려 보냅니다
  useLayoutEffect(() => {
    if (AuthController.getToken() === null) return;
    navigate('/my-page');
  }, []);

  const handleFinish = (data: any) => {
    setIsSignUpLoading(true);

    postSignupMutation
      .mutateAsync(data)
      .then(res => {
        const { code } = res;
        if (code === 'user_created') {
          toast.success('회원가입에 성공했습니다!', {
            duration: 3000,
          });
          navigate('/login');
        }
      })
      .catch(err => {
        const { code, message } = err.response.data;
        if (code && message && code === 'email_exists') {
          toast.error(message, {
            duration: 3000,
          });
          return;
        }
        toast.error('회원가입에 실패했습니다!', {
          duration: 3000,
        });
      })
      .finally(() => {
        setIsSignUpLoading(false);
      });
  };

  const handleMoveToLoginPageButton = () => {
    navigate('/login');
  };

  const checkPassword = (_: any, value: string) => {
    if (!value) return Promise.reject(new Error('비밀번호를 입력해주세요!'));
    return form.getFieldValue('password_1') === value
      ? Promise.resolve()
      : Promise.reject(new Error('비밀번호가 일치하지 않습니다!'));
  };

  return (
    <div className="flex flex-col justify-center">
      <div className="p-5">
        <img
          src="https://www.apmmust.com/wp-content/uploads/2022/12/apm-Must-320x71.png"
          className="logo dark-logo m-auto"
          alt="apM MUST"
          width="160"
          height="35.5"
        ></img>
      </div>
      <Form
        form={form}
        footer={
          <>
            <Button block type="submit" color="primary" size="large" loading={isSignUpLoading}>
              회원가입
            </Button>
            <div className="flex justify-end">
              <Button color="primary" fill="none" onClick={handleMoveToLoginPageButton} disabled={isSignUpLoading}>
                로그인
              </Button>
            </div>
          </>
        }
        onFinish={handleFinish}
      >
        {/* 이메일 */}
        <Form.Item label="이메일" name="email" rules={[{ required: true, message: '이메일을 입력해주세요!' }]}>
          <Input placeholder="이메일을 입력해주세요" />
        </Form.Item>
        {/* 비밀번호 */}
        <Form.Item label="비밀번호" name="password_1" rules={[{ required: true, message: '비밀번호를 입력해주세요!' }]}>
          <Input placeholder="비밀번호를 입력해주세요" />
        </Form.Item>
        {/* 비밀번호 재입력 */}
        <Form.Item label="비밀번호 재입력" name="password_2" rules={[{ validator: checkPassword }]}>
          <Input placeholder="비밀번호를 다시 입력해주세요" />
        </Form.Item>
        {/* 브랜드 */}
        <Form.Item label="브랜드" name="brand" rules={[{ required: true, message: '브랜드를 입력해주세요' }]}>
          <Input placeholder="브랜드를 입력해주세요" />
        </Form.Item>
        {/* 건물 */}
        <Form.Item label="건물" name="building" rules={[{ required: true, message: '건물을 입력해주세요' }]}>
          <Input placeholder="브랜드를 입력해주세요" />
        </Form.Item>
        {/* 층 */}
        <Form.Item label="층" name="floor" rules={[{ required: true, message: '층을 입력해주세요' }]}>
          <Input placeholder="브랜드를 입력해주세요" />
        </Form.Item>
        {/* 호수 */}
        <Form.Item label="호수" name="room_number" rules={[{ required: true, message: '호수를 입력해주세요' }]}>
          <Input placeholder="브랜드를 입력해주세요" />
        </Form.Item>
        {/* 은행 */}
        <Form.Item label="은행" name="bank" rules={[{ required: true, message: '은행을 입력해주세요' }]}>
          <Input placeholder="은행을 입력해주세요" />
        </Form.Item>
        {/* 입금 계좌번호 */}
        <Form.Item
          label="입금 계좌번호"
          name="deposit_account"
          rules={[{ required: true, message: '입금 계좌번호를 입력해주세요!' }]}
        >
          <Input placeholder="입금 계좌번호를 입력해주세요" />
        </Form.Item>
        {/* 매장 폰 번호 */}
        <Form.Item
          label="매장 폰 번호"
          name="phone_number"
          rules={[{ required: true, message: '매장 폰 번호를 입력해주세요!' }]}
        >
          <Input placeholder="매장 폰번호를 입력해주세요" />
        </Form.Item>
      </Form>
    </div>
  );
};

export default SignUp;
