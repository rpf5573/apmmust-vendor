import useGetProductTerms, { ApiTerms } from '@api/api-terms';
import { Button, CheckList, Form, Popup, SearchBar, Space, Tag } from 'antd-mobile';
import { FormInstance } from 'rc-field-form';
import { useEffect, useMemo, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

export const colour_form_item_name = 'colours';

type ColourFormItemProps = {
  terms: ApiTerms['get']['responseData']['data']['colours'];
  termMap: ApiTerms['get']['responseData']['data']['colour_map'];
};

const ColourFormItem: React.FC<ColourFormItemProps> = ({ terms: colours, termMap: colourMap }) => {
  const form = useFormContext();
  const [selected, setSelected] = useState<string[]>([]);
  const [visible, setVisible] = useState(false);
  const [searchText, setSearchText] = useState('');

  const colourNamesForSearch = useMemo(() => {
    return colours.map(item => item.name.toLocaleLowerCase().replace(/\s+/g, ''));
  }, [colours]);

  const filteredItems = useMemo(() => {
    return searchText
      ? colours.filter((_, index) =>
          colourNamesForSearch[index].includes(searchText.toLocaleLowerCase().replace(/\s+/g, '')),
        )
      : colours;
  }, [colours, searchText]);

  // selected가 바뀌면 form에도 업데이트
  useEffect(() => {
    form.setValue(colour_form_item_name, selected);
  }, [selected]);

  const handleColourChange = (val: Array<string>) => {
    setVisible(false);
    setSelected(val);
  };

  return (
    <Controller
      name={colour_form_item_name}
      control={form.control}
      render={({ field: { onChange, value, name }, fieldState: { invalid, error } }) => (
        <CustomFormItem label="색상" showError={invalid} errorMessage={error?.message} required>
          <Space align="center">
            <Button
              onClick={() => {
                setVisible(true);
              }}
            >
              선택
            </Button>
            <div className="flex gap-x-1">
              {selected.map(term_id => (
                <Tag key={term_id} color="primary" fill="outline" className="text-[14px]">
                  {colourMap[term_id]?.name}
                </Tag>
              ))}
            </div>
          </Space>
          <Popup
            className="form-item-content--colour__popup"
            visible={visible}
            onMaskClick={() => {
              setVisible(false);
            }}
            destroyOnClose
          >
            <div className="search-bar-container p-[12px]">
              <SearchBar
                placeholder="색상 검색"
                value={searchText}
                onChange={v => {
                  setSearchText(v);
                }}
              />
            </div>
            <div className="check-list-container">
              <CheckList multiple defaultValue={[...selected]} onChange={handleColourChange}>
                {filteredItems.map(item => (
                  <CheckList.Item key={item.term_id} value={`${item.term_id}`}>
                    {item.name}
                  </CheckList.Item>
                ))}
              </CheckList>
            </div>
          </Popup>
        </CustomFormItem>
      )}
      rules={{ required: '색상을 입력해주세요' }}
    />
  );
};

export default ColourFormItem;
