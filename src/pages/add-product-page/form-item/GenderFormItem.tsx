import useGetProductTerms, { ApiTerms } from '@api/api-terms';
import { Gender } from '@custom-types';
import { Form, Selector } from 'antd-mobile';
import { atom, useAtomValue, useSetAtom } from 'jotai';
import { FormInstance } from 'rc-field-form';
import { useEffect, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

export const atom_gender = atom<Gender>('c-unisex');

//@TODO: 성별 선택하면 카테고리 초기화 되도록

const gender_form_item_name = 'genders';

type GenderFormItemProps = {
  terms: ApiTerms['get']['responseData']['data']['genders'];
  termMap: ApiTerms['get']['responseData']['data']['gender_map'];
};

const GenderFormItem: React.FC<GenderFormItemProps> = ({ terms: genders, termMap: genderMap }) => {
  const form = useFormContext();
  const [selected, setSelected] = useState<string[]>([]);

  const options = genders.map(item => ({
    label: item.name,
    value: `${item.term_id}`,
  }));

  const setGender = useSetAtom(atom_gender);

  // selected가 바뀌면 form에도 업데이트
  useEffect(() => {
    form.setValue(gender_form_item_name, selected);
  }, [selected]);

  const handleChange = (val: Array<string>) => {
    if (val.length === 0) return;
    const genderId = val[0];
    const genderName = genders.filter(item => item.term_id === Number(genderId))[0].slug; // 이거는 솔직히 어거지로 맞춘거긴 함. 하지만 앞으로 변할일은 없을것 같아.
    setGender(genderName as Gender);
    setSelected([genderId]);
    form.clearErrors(gender_form_item_name);
  };

  return (
    <Controller
      name={gender_form_item_name}
      control={form.control}
      render={({ field: { onChange, value, name }, fieldState: { invalid, error } }) => (
        <CustomFormItem label="성별" required showError={invalid} errorMessage={error?.message}>
          <Selector
            options={options}
            value={value}
            onChange={v => {
              onChange(v); // 이건 컨트롤에게 알리는것
              handleChange(v); // 이건 내 커스텀 처리
            }}
          />
        </CustomFormItem>
      )}
      rules={{ required: '성별을 선택해주세요' }}
    />
  );
};

export default GenderFormItem;
