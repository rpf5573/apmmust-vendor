import { Form, Input } from 'antd-mobile';
import { Controller, useFormContext } from 'react-hook-form';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

const name_form_item = 'name';

const NameFormItem = () => {
  const form = useFormContext();

  return (
    <Controller
      name={name_form_item}
      control={form.control}
      render={({ field: { ref, ...field }, fieldState: { invalid, error } }) => {
        return (
          <CustomFormItem label="상품명" required showError={invalid} errorMessage={error?.message}>
            <Input placeholder="상품 이름 입력" {...field} ref={ref} />
          </CustomFormItem>
        );
      }}
      rules={{ required: '상품명을 입력해주세요' }}
    />
  );
};

export default NameFormItem;
