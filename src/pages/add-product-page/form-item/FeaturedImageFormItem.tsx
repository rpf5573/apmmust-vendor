// Step 1: Import the S3Client object and all necessary SDK commands.
import { apmmustS3Client, bucket, getImageUrl } from '@api/api-s3';
import { PutObjectCommand } from '@aws-sdk/client-s3';
import atom_imageUploadStatus from '@global-state/image-upload-status';
import { generateProductFilename } from '@utils';
import { Button, Dialog, Form, ImageUploader, Input, Space, Swiper, Tabs, Toast } from 'antd-mobile';
import type { ImageUploadItem } from 'antd-mobile';
import { UploadTask } from 'antd-mobile/es/components/image-uploader';
import { useAtom, useSetAtom } from 'jotai';
import { FormInstance } from 'rc-field-form';
import { useEffect, useRef, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { toast } from 'react-hot-toast';
import _ from 'underscore';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

const featured_form_item_name = 'featured_image_url';

function FeaturedImageFormItem() {
  const form = useFormContext();
  const [selected, setSelected] = useState<Array<ImageUploadItem>>([]);
  const setImageUploadStatus = useSetAtom(atom_imageUploadStatus);

  // selected가 바뀌면 form에도 업데이트
  useEffect(() => {
    form.setValue(
      featured_form_item_name,
      selected.map(file => file.url),
    );
    form.clearErrors(featured_form_item_name);
  }, [selected]);

  async function handleUpload(file: any) {
    setImageUploadStatus({ isFeaturedImageUploading: true });

    const key = generateProductFilename(file, 'vendor');

    // Step 3: Define the parameters for the object you want to upload.
    const params = {
      Bucket: bucket, // The path to the directory you want to upload the object to, starting with your Space name.
      Key: key, // Object key, referenced whenever you want to access this file later.
      Body: file, // The object's contents. This variable is an object, not a string.
      ACL: 'public-read', // Defines ACL permissions, such as private or public.
      Metadata: {},
    };
    try {
      const data = await apmmustS3Client.send(new PutObjectCommand(params));
      setImageUploadStatus({ isFeaturedImageUploading: false }); // 이미지 업로드에 성공한 경우에만 등록하기 가능함

      const url = getImageUrl(params.Key);
      return { url };
    } catch (err) {
      toast.error('이미지 업로드 실패');
      throw new Error('이미지 업로드 실패');
    }
  }

  // 업로드 실패한 이미지를 삭제할때 '등록하기' 버튼도 다시 활성화 되도록 한다
  const handleUploadQueueChange = (tasks: UploadTask[]) => {
    if (tasks.filter(task => task.status === 'pending').length > 0) return;
    if (tasks.filter(task => task.status === 'fail').length > 0) return;

    setImageUploadStatus({ isFeaturedImageUploading: false });
  };

  return (
    <Controller
      name={featured_form_item_name}
      control={form.control}
      render={({ field, fieldState: { invalid, error } }) => (
        <CustomFormItem label="대표 이미지" required showError={invalid} errorMessage={error?.message}>
          <ImageUploader
            value={selected}
            maxCount={1}
            onChange={setSelected}
            upload={handleUpload}
            onUploadQueueChange={handleUploadQueueChange}
          />
        </CustomFormItem>
      )}
      rules={{ required: '대표 이미지를 선택해주세요' }}
    />
  );
}

export default FeaturedImageFormItem;
