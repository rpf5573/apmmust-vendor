import useGetProductTerms, { ApiTerms } from '@api/api-terms';
import { Button, CheckList, Form, Input, Popup, SearchBar, Space, Tag } from 'antd-mobile';
import { useEffect, useMemo, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

const season_form_item_name = 'seasons';

type SeasonFormItemProps = {
  terms: ApiTerms['get']['responseData']['data']['seasons'];
  termMap: ApiTerms['get']['responseData']['data']['season_map'];
};

const SeasonFormItem: React.FC<SeasonFormItemProps> = ({ terms: seasons, termMap: seasonMap }) => {
  const form = useFormContext();
  const getTermsQuery = useGetProductTerms();
  const [visible, setVisible] = useState(false);
  const [selected, setSelected] = useState<string[]>([]);
  const [searchText, setSearchText] = useState('');

  const seasoNamesForSearch = useMemo(() => {
    return seasons.map(item => item.name.toLocaleLowerCase().replace(/\s+/g, ''));
  }, [seasons]);

  const filteredItems = useMemo(() => {
    return searchText
      ? seasons.filter((_, index) =>
          seasoNamesForSearch[index].includes(searchText.toLocaleLowerCase().replace(/\s+/g, '')),
        )
      : seasons;
  }, [seasons, searchText]);

  // selected가 바뀌면 form에도 업데이트
  useEffect(() => {
    form.setValue(season_form_item_name, selected);
    form.clearErrors(season_form_item_name);
  }, [selected]);

  if (seasons.length === 0) return null;

  const handleSeasonChange = (val: Array<string>) => {
    setVisible(false);
    setSelected(val.length > 0 ? [val[0]] : []);
  };

  return (
    <Controller
      name={season_form_item_name}
      control={form.control}
      render={({ field: { onChange, value, name }, fieldState: { invalid, error } }) => (
        <CustomFormItem label="시즌" required showError={invalid} errorMessage={error?.message}>
          <div>
            <Space align="center">
              <Button
                onClick={() => {
                  setVisible(true);
                }}
              >
                선택
              </Button>
              <div className="flex gap-x-1">
                {selected.map(term_id => (
                  <Tag key={term_id} color="primary" fill="outline">
                    {seasonMap[term_id]?.name}
                  </Tag>
                ))}
              </div>
            </Space>
            <Popup
              visible={visible}
              onMaskClick={() => {
                setVisible(false);
              }}
              destroyOnClose
            >
              <div className="p-[12px]">
                <SearchBar
                  placeholder="시즌 검색"
                  value={searchText}
                  onChange={v => {
                    setSearchText(v);
                  }}
                />
              </div>
              <div>
                <CheckList defaultValue={[...selected]} onChange={handleSeasonChange}>
                  {filteredItems.map(({ term_id, name }) => (
                    <CheckList.Item key={term_id} value={`${term_id}`}>
                      {name}
                    </CheckList.Item>
                  ))}
                </CheckList>
              </div>
            </Popup>
          </div>
        </CustomFormItem>
      )}
      rules={{ required: '시즌을 입력해주세요' }}
    />
  );
};

export default SeasonFormItem;
