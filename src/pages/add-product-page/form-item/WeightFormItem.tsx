import { Button, CheckList, Form, Input, Popup, SearchBar, Space, Tag } from 'antd-mobile';
import { Controller, useFormContext } from 'react-hook-form';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

function WeightFormItem() {
  const { control } = useFormContext();

  return (
    <Controller
      name="weight"
      control={control}
      render={({ field, fieldState: { invalid, error } }) => (
        <CustomFormItem label="상품 무게" showError={invalid} errorMessage={error?.message}>
          <Input placeholder="무게 입력" {...field} />
        </CustomFormItem>
      )}
    />
  );
}

export default WeightFormItem;
