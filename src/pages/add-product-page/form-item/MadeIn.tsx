import useGetProductTerms, { ApiTerms } from '@api/api-terms';
import { Button, CheckList, Form, Input, Popup, SearchBar, Space, Tag } from 'antd-mobile';
import { FormInstance } from 'rc-field-form';
import { useEffect, useMemo, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

const made_in_form_item_name = 'made_ins';

type MadeInFormItemProps = {
  terms: ApiTerms['get']['responseData']['data']['made_ins'];
  termMap: ApiTerms['get']['responseData']['data']['made_in_map'];
};

const MadeInFormItem: React.FC<MadeInFormItemProps> = ({ terms: madeIns, termMap: madeInMap }) => {
  const form = useFormContext();

  const [visible, setVisible] = useState(false);
  const [selected, setSelected] = useState<string[]>([]);

  // selected가 바뀌면 form에도 업데이트
  useEffect(() => {
    form.setValue(made_in_form_item_name, selected);
    form.clearErrors(made_in_form_item_name);
  }, [selected]);

  const handleMadeInChange = (val: Array<string>) => {
    setVisible(false);
    setSelected(val.length > 0 ? [val[0]] : []);
  };

  return (
    <Controller
      name={made_in_form_item_name}
      control={form.control}
      render={({ field: { onChange, value, name }, fieldState: { invalid, error } }) => (
        <CustomFormItem label="제조국가" showError={invalid} errorMessage={error?.message} required>
          <div>
            <Space align="center">
              <Button
                onClick={() => {
                  setVisible(true);
                }}
              >
                선택
              </Button>
              <div className="flex gap-x-1">
                {selected.map(term_id => (
                  <Tag key={term_id} color="primary" fill="outline" className="text-[14px]">
                    {madeInMap[term_id]?.name}
                  </Tag>
                ))}
              </div>
            </Space>
            <Popup
              className="form-item-content--size__popup"
              visible={visible}
              onMaskClick={() => {
                setVisible(false);
              }}
              destroyOnClose
            >
              <div className="check-list-container">
                <CheckList defaultValue={[...selected]} onChange={handleMadeInChange}>
                  {madeIns.map(({ term_id, name }) => (
                    <CheckList.Item key={term_id} value={`${term_id}`}>
                      {name}
                    </CheckList.Item>
                  ))}
                </CheckList>
              </div>
            </Popup>
          </div>
        </CustomFormItem>
      )}
      rules={{ required: '제조국가를 입력해주세요' }}
    />
  );
};

export default MadeInFormItem;
