import { FABRICS } from '@constants';
import { Button, CheckList, Form, Input, Popup, Space } from 'antd-mobile';
import { CloseCircleFill } from 'antd-mobile-icons';
import { useEffect, useState } from 'react';
import { Controller, useFieldArray, useFormContext } from 'react-hook-form';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

const mixing_rate_form_item_name = 'mixing_rates';

const MixingRateFormItem: React.FC = () => {
  const form = useFormContext();

  const valueToLabel = FABRICS.reduce((acc, { label, value }) => {
    acc[value] = label;
    return acc;
  }, {} as any);

  const [visible, setVisible] = useState(false);
  const [selectedFabrics, setSelectedFabrics] = useState<Array<string>>([]); // value만 담겨있다

  // 선택받지 못한 field들은 전부 unregister한다
  useEffect(() => {
    FABRICS.filter(item => {
      return !selectedFabrics.includes(item.value);
    }).forEach(({ value }) => {
      form.unregister(value);
    });
  }, [selectedFabrics]);

  return (
    <CustomFormItem label="혼합률">
      <Space align="center">
        <Button
          onClick={() => {
            setVisible(true);
          }}
        >
          옷감선택
        </Button>
      </Space>
      <Popup
        visible={visible}
        onMaskClick={() => {
          setVisible(false);
        }}
        destroyOnClose
      >
        <div className="check-list-container">
          <CheckList
            defaultValue={['']}
            onChange={val => {
              setSelectedFabrics(prev => {
                return [...new Set([...prev, ...val])];
              });
              setVisible(false);
            }}
          >
            {FABRICS.filter(item => {
              return !selectedFabrics.includes(item.value);
            }).map(item => (
              <CheckList.Item key={item.value} value={item.value}>
                {item.label}
              </CheckList.Item>
            ))}
          </CheckList>
        </div>
      </Popup>
      {selectedFabrics.map((fabric, index) => (
        <div className="wrapper" key={`${fabric}-${index}`}>
          <Controller
            name={fabric}
            control={form.control}
            render={({ field, fieldState: { invalid, error } }) => {
              return (
                <CustomFormItem label={valueToLabel[fabric]} showError={invalid} errorMessage={error?.message} required>
                  <div className="flex justify-between items-center">
                    <div className="grow-1 flex">
                      <Input placeholder="0" min={0} max={100} maxLength={3} type="number" {...field} />
                      <span>%</span>
                    </div>
                    <div>
                      <Button
                        className="pure-button"
                        onClick={() => {
                          setSelectedFabrics(prev => prev.filter(_fabric => _fabric !== fabric));
                        }}
                      >
                        <CloseCircleFill fontSize={24} color="var(--adm-color-danger)" />
                      </Button>
                    </div>
                  </div>
                </CustomFormItem>
              );
            }}
            rules={{ required: '혼합률을 입력해주세요' }}
          />
          <style jsx>{`
            .wrapper :global(.adm-list-item-content-main) {
              display: flex;
              align-items: center;
              column-gap: 20px;
              :global(.adm-form-item-child) {
                flex-grow: 1;
              }
              :global(.adm-form-item-label) {
                margin-bottom: 0;
              }
              :global(.pure-button) {
                background-color: transparent !important;
                border: transparent;
              }
            }
          `}</style>
        </div>
      ))}
    </CustomFormItem>
  );
};

export default MixingRateFormItem;
