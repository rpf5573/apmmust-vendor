import { Form, Input } from 'antd-mobile';
import { Controller, useFormContext } from 'react-hook-form';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

type SalePriceFormItemProps = {
  value?: string;
};

const SalePriceFormItem = ({ value }: SalePriceFormItemProps) => {
  const { control } = useFormContext();

  return (
    <Controller
      name="sale_price"
      control={control}
      render={({ field, fieldState: { invalid, error } }) => (
        <CustomFormItem label="할인 가격" showError={invalid} errorMessage={error?.message}>
          <Input type="number" placeholder="할인된 상품 가격 입력" {...field} />
        </CustomFormItem>
      )}
    />
  );
};

export default SalePriceFormItem;
