import { Form, Input } from 'antd-mobile';
import { Controller, useFormContext } from 'react-hook-form';

import CustomFormItem from '@components/custom-form-item/CustomFormItem';

const PriceFormItem = () => {
  const { control } = useFormContext();

  return (
    <Controller
      name="regular_price"
      control={control}
      render={({ field, fieldState: { invalid, error } }) => (
        <CustomFormItem label="가격" required showError={invalid} errorMessage={error?.message}>
          <Input type="number" placeholder="상품 가격 입력" {...field} />
        </CustomFormItem>
      )}
      rules={{ required: '가격을 입력해주세요' }}
    />
  );
};

export default PriceFormItem;
