import { usePostProduct } from '@api/api-post-product';
import useGetProductTerms from '@api/api-terms';
import { FABRICS } from '@constants';
import atom_imageUploadStatus from '@global-state/image-upload-status';
import withAuth from '@hoc/withAuth';
import useLoading from '@hooks/useLoading';
import { AppScreen } from '@stackflow/plugin-basic-ui';
import { ActivityComponentType } from '@stackflow/react';
import { Button, List, Mask, SpinLoading } from 'antd-mobile';
import { useAtomValue } from 'jotai';
import { useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { toast } from 'react-hot-toast';
import { useNavigate } from 'react-router-dom';
import _ from 'underscore';

import GlobalErrorView from '@components/@shared/GlobalErrorView';

import CategoryFormItem from './form-item/CategoryFormItem';
import ColourFormItem from './form-item/ColourFormItem';
import FeaturedImageFormItem from './form-item/FeaturedImageFormItem';
import GalleryImageFormItem from './form-item/GalleryImageFormItem';
import GenderFormItem from './form-item/GenderFormItem';
import MadeInFormItem from './form-item/MadeIn';
import MixingRateFormItem from './form-item/MixingRateFormItem';
import NameFormItem from './form-item/NameFormItem';
import PriceFormItem from './form-item/PriceFormItem';
import SalePriceFormItem from './form-item/SalePriceFormItem';
import SeasonFormItem from './form-item/SeasonFormItem';
import SizeFormItem from './form-item/SizeFormItem';
import WeightFormItem from './form-item/WeightFormItem';

const AddProduct: ActivityComponentType = ({ params }) => {
  const form = useForm();

  const [isMakingProduct, setIsMakingProduct] = useState(false);
  const imageUploadStatus = useAtomValue(atom_imageUploadStatus);

  const postProduct = usePostProduct();
  const navigate = useNavigate();

  const getProductTermsQuery = useGetProductTerms();
  useLoading(getProductTermsQuery.isLoading);
  if (getProductTermsQuery.isLoading) return null;

  const data = getProductTermsQuery.data!.data;

  const onSubmit = (data: any) => {
    setIsMakingProduct(true);

    if (!data) return;
    if (!_.isObject(data)) return;

    FABRICS.forEach(fabric => {
      if (data[fabric.value]) {
        if (!data['mixing_rates']) {
          data['mixing_rates'] = {};
        }
        data['mixing_rates'][fabric.value] = data[fabric.value];
        delete data[fabric.value];
      }
    });

    postProduct
      .mutateAsync(data as any)
      .then(res => {
        toast.success(res.message);
        navigate('/products', { replace: true });
      })
      .catch(err => {
        console.error(err);
        toast.error('상품 생성에 실패했습니다');
      })
      .finally(() => {
        setIsMakingProduct(false);
      });
  };

  return (
    <AppScreen appBar={{ title: '상품 등록' }}>
      <FormProvider {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)} className="adm-form pb-[50px]">
          <List>
            <NameFormItem />
            <PriceFormItem />
            <SalePriceFormItem />
            <WeightFormItem />
            <SizeFormItem terms={data.sizes} termMap={data.size_map} />
            <ColourFormItem terms={data.colours} termMap={data.colour_map} />
            <FeaturedImageFormItem />
            <GalleryImageFormItem />
            <GenderFormItem terms={data.genders} termMap={data.gender_map} />
            <CategoryFormItem terms={data.categories} termMap={data.category_map} />
            <MixingRateFormItem />
            <SeasonFormItem terms={data.seasons} termMap={data.season_map} />
            <MadeInFormItem terms={data.made_ins} termMap={data.made_in_map} />
          </List>
          <div className="adm-form-footer">
            <Button
              block
              type="submit"
              color="primary"
              size="large"
              disabled={
                imageUploadStatus.isFeaturedImageUploading === true ||
                imageUploadStatus.isGalleryImageUploading === true ||
                Object.keys(form.formState.errors).length > 0
              }
              loading={isMakingProduct}
            >
              등록하기
            </Button>
          </div>
        </form>
      </FormProvider>
    </AppScreen>
  );
};

const Loading: React.FC = () => {
  return (
    <div className="loading-container">
      <Mask visible={true} opacity={'thin'}>
        <SpinLoading color="primary" />
      </Mask>
      <style jsx>{`
        .loading-container :global(.adm-mask) {
          display: flex;
          justify-content: center;
          align-items: center;
        }
      `}</style>
    </div>
  );
};

export default withAuth(AddProduct);
