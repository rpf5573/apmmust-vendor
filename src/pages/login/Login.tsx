import { usePostLogin } from '@api/api-login';
import AuthController from '@auth/AuthController';
import { ERROR_CODE, UNKNOWN_ERROR } from '@constants';
import withAuth from '@hoc/withAuth';
import { Button, Form, Input, Toast } from 'antd-mobile';
import React from 'react';
import { useState } from 'react';
import { toast } from 'react-hot-toast';
import { useNavigate } from 'react-router-dom';
import _ from 'underscore';

const Login: React.FC = () => {
  const { mutateAsync: login } = usePostLogin();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [form] = Form.useForm();
  const navigate = useNavigate();

  const handleSubmit = () => {
    setIsLoading(true);

    const values = form.getFieldsValue();
    const isValid = _.isObject(values) && _.has(values, 'id') && _.has(values, 'password');
    if (!isValid) return;
    const { id: username, password } = values;

    login({ username, password })
      .then(response => {
        try {
          AuthController.saveUserData(response);
          navigate('/products');
        } catch (err) {
          console.error(err);
          toast.error('ERROR(001) - 오류가 발생했습니다');
        }
      })
      .catch(err => {
        const isValid =
          _.isObject(err) && _.has(err, 'response') && _.has(err.response, 'data') && _.has(err.response.data, 'code');
        if (!isValid) {
          throw new Error('ERROR(002) - 오류 객체에 문제가 있습니다');
        }
        const code = err.response.data.code;

        if (_.has(ERROR_CODE, code)) {
          toast.error(ERROR_CODE[code as string]);
        } else {
          toast.error(UNKNOWN_ERROR);
        }
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const handleMoveToSignupPageButton = () => {
    navigate('/sign-up');
  };

  return (
    <div className="flex flex-col justify-center h-[100vh] pb-40">
      <div className="p-5">
        <img
          src="https://www.apmmust.com/wp-content/uploads/2022/12/apm-Must-320x71.png"
          className="logo dark-logo m-auto"
          alt="apM MUST"
          width="160"
          height="35.5"
        ></img>
      </div>
      <Form
        form={form}
        layout="horizontal"
        footer={
          <>
            <Button loading={isLoading} block color="primary" size="large" className="mt-1" onClick={handleSubmit}>
              로그인
            </Button>
            <div className="flex justify-end">
              <Button color="primary" fill="none" onClick={handleMoveToSignupPageButton}>
                회원가입
              </Button>
            </div>
          </>
        }
      >
        <Form.Item name="id" label="이메일" rules={[{ required: true, message: '필수 항목입니다' }]}>
          <Input placeholder="이메일" />
        </Form.Item>
        <Form.Item name="password" label="비밀번호" rules={[{ required: true, message: '필수 항목입니다' }]}>
          <Input placeholder="비밀번호" type="password" />
        </Form.Item>
      </Form>
    </div>
  );
};

export default withAuth(Login);
