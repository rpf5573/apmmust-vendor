import axiosInstance from '@api/axiosInstance';
import { API_URL } from '@constants';
import { Notice } from '@custom-types';
import { useQuery } from '@tanstack/react-query';
import { AxiosResponse } from 'axios';

export type ApiNotice = {
  get: {
    responseData: {
      code: string;
      data: Notice;
      message: string;
    };
  };
  post: {};
};

// get product
export const getNotice = async (noticeId: string) => {
  const response = await axiosInstance.get<
    ApiNotice['get']['responseData'],
    AxiosResponse<ApiNotice['get']['responseData']>
  >(`${API_URL}/notices/get-notice?notice_id=${noticeId}`);

  console.log('response.data - get_notice', response.data);

  return response.data;
};

export const useGetNotice = (noticeId: string) =>
  useQuery<ApiNotice['get']['responseData']>({
    queryKey: ['get-notice', noticeId],
    queryFn: () => getNotice(noticeId),
  });

export default useGetNotice;

export type ApiNotices = {
  get: {
    params: {
      page: number;
      postsPerPage: number;
    };
    responseData: {
      code: string;
      data: Array<Notice>;
      message: string;
      current_page: number;
      total_posts: number;
    };
  };
};

export const getNotices = async ({ page, postsPerPage }: ApiNotices['get']['params']) => {
  const response = await axiosInstance.get<
    ApiNotices['get']['responseData'],
    AxiosResponse<ApiNotices['get']['responseData']>
  >(`${API_URL}/notices/get-notices?page=${page}&posts_per_page=${postsPerPage}`);

  console.log('response.data', response.data);

  return response.data;
};

export const useGetNotices = ({ page, postsPerPage }: ApiNotices['get']['params']) =>
  useQuery<ApiNotices['get']['responseData']>({
    queryKey: ['get-notices', page, postsPerPage],
    queryFn: () => getNotices({ page, postsPerPage }),
  });
