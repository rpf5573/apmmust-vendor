import axiosInstance from '@api/axiosInstance';
import { API_URL } from '@constants';
import type { DefaultError } from '@tanstack/query-core';
import { useMutation } from '@tanstack/react-query';
import { AxiosError, AxiosResponse } from 'axios';

export type ApiLogin = {
  post: {
    params: { username: string; password: string };
    responseData: {
      token: string;
      user_display_name: string;
      user_email: string;
      user_nicename: string;
    };
    variables: ApiLogin['post']['params'];
  };
};

// login
export const postLogin = async ({ username, password }: ApiLogin['post']['variables']) => {
  const response = await axiosInstance.post<
    ApiLogin['post']['responseData'],
    AxiosResponse<ApiLogin['post']['responseData']>,
    ApiLogin['post']['variables']
  >(`${API_URL}/auth/token`, { username, password });

  return response.data;
};

export const usePostLogin = () =>
  useMutation<ApiLogin['post']['responseData'], DefaultError, ApiLogin['post']['variables']>({
    mutationFn: postLogin,
  });

export default usePostLogin;
