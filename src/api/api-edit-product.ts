import axiosInstance from '@api/axiosInstance';
import { API_URL, FABRICS } from '@constants';
import { DefaultError, useMutation, useQuery } from '@tanstack/react-query';
import { AxiosError, AxiosResponse } from 'axios';

export type ApiEditProduct = {
  post: {
    params: {
      name: string;
      regular_price: string;
      sale_price?: string;
      weight?: string;
      sizes?: Array<number>;
      colours?: Array<number>;
      featured_image_url: string;
      gallery_image_urls?: Array<string>;
      gender?: Array<number>;
      categories?: Array<number>;
      mixing_rates: {
        [key in (typeof FABRICS)[number]['value']]?: number;
      };
    };
    responseData: {
      code: string;
      data: any;
      message: string;
    };
    variables: ApiEditProduct['post']['params'];
  };
};

// post product
export const editProduct = async (product: ApiEditProduct['post']['variables']) => {
  const response = await axiosInstance.post<
    ApiEditProduct['post']['responseData'],
    AxiosResponse<ApiEditProduct['post']['responseData']>
  >(`${API_URL}/products/edit-product`, product);

  return response.data;
};

export const useEditProduct = () => {
  return useMutation<ApiEditProduct['post']['responseData'], DefaultError, ApiEditProduct['post']['variables']>({
    mutationFn: editProduct,
  });
};
