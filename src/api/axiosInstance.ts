import AuthController from '@auth/AuthController';
import AccessTokenController from '@auth/AuthController';
import { API_URL, PATH } from '@constants';
import axios, { type AxiosError, type AxiosResponse } from 'axios';

const axiosConfig = {
  baseURL: API_URL,
  headers: {
    'Content-Type': 'application/json',
  },
};

const axiosInstance = axios.create(axiosConfig);

const handleAxiosError = (error: AxiosError<{ message: string; code?: number }>) => {
  console.error(error);

  if (error.response?.status === 403) {
    AccessTokenController.clear();
    // window.location.replace(PATH.MAIN);
    return Promise.reject(error);
  }

  const data = error.response?.data;
  if (data?.message) {
    console.error(data.message);
    // TODO: 커스텀 에러 코드를 만들어서 그에 맞는 message를 담은 error 객체를 return 하도록 해야 함
    return Promise.reject(error);
  }

  error.message = '알 수 없는 오류가 발생했습니다. 잠시 후 다시 시도해주세요 :(';
  return Promise.reject(error);
};

const handleAxiosResponse = (response: AxiosResponse) => {
  // 서버에서 아무 응답 데이터도 오지 않으면 빈 스트링 ''이 오므로 명시적으로 null로 지정
  if (response.data !== '') return response;

  response.data = null;
  return response;
};

axiosInstance.interceptors.response.use(handleAxiosResponse, handleAxiosError);

axiosInstance.interceptors.request.use(
  async config => {
    const token = AuthController.getToken();
    if (!token) return config;

    config.headers['Authorization'] = `Bearer ${token}`;
    return config;
  },
  error => {
    return Promise.reject(error);
  },
);

export { axiosInstance as default };
