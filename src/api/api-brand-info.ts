import axiosInstance from '@api/axiosInstance';
import { API_URL } from '@constants';
import { DefaultError, useMutation, useQuery } from '@tanstack/react-query';
import { AxiosError, AxiosResponse } from 'axios';

export type ApiBrandInfo = {
  get: {
    responseData: {
      code: string;
      message: string;
      data: {
        status: number;
        brand_id: string;
        brand_name: string;
        building: string;
        floor: string;
        room_number: string;
      };
    };
  };
};

// get product
export const getBrandInfo = async (brandId: string) => {
  const response = await axiosInstance.get<
    ApiBrandInfo['get']['responseData'],
    AxiosResponse<ApiBrandInfo['get']['responseData']>
  >(`${API_URL}/products/brand-info/?brand_id=${brandId}`);

  return response.data;
};

export const useGetBrandInfo = (brandId: string, enabled: boolean = true) =>
  useQuery<ApiBrandInfo['get']['responseData']>({
    queryKey: ['get-brand-info', brandId],
    queryFn: () => getBrandInfo(brandId),
    gcTime: 0,
    staleTime: 0,
    enabled,
  });

export default useGetBrandInfo;
