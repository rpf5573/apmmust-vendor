import axiosInstance from '@api/axiosInstance';
import { API_URL } from '@constants';
import { MixingRate, Product, Term } from '@custom-types';
import { DefaultError, useMutation, useQuery } from '@tanstack/react-query';
import { AxiosError, AxiosResponse } from 'axios';

export type ApiDeleteProduct = {
  delete: {
    responseData: {
      code: string;
      data: Product;
      message: string;
    };
  };
};

// delete product
export const deleteProduct = async (product_id: string) => {
  const url = `${API_URL}/products/delete-product?product_id=${product_id}`;
  const response = await axiosInstance.delete<
    ApiDeleteProduct['delete']['responseData'],
    AxiosResponse<ApiDeleteProduct['delete']['responseData']>
  >(url);

  return response.data;
};
