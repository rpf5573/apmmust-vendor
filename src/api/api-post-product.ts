import axiosInstance from '@api/axiosInstance';
import { API_URL, FABRICS } from '@constants';
import { DefaultError, useMutation, useQuery } from '@tanstack/react-query';
import { AxiosError, AxiosResponse } from 'axios';

export type ApiProductPost = {
  post: {
    params: {
      name: string;
      regular_price: string;
      sale_price?: string;
      weight?: string;
      sizes?: Array<number>;
      colours?: Array<number>;
      featured_image_url: string;
      gallery_image_urls?: Array<string>;
      gender?: Array<number>;
      categories?: Array<number>;
      mixing_rates: {
        [key in (typeof FABRICS)[number]['value']]?: number;
      };
    };
    responseData: {
      code: string;
      data: any;
      message: string;
    };
    variables: ApiProductPost['post']['params'];
  };
};

// post product
export const postProduct = async (product: ApiProductPost['post']['variables']) => {
  const response = await axiosInstance.post<
    ApiProductPost['post']['responseData'],
    AxiosResponse<ApiProductPost['post']['responseData']>
  >(`${API_URL}/products/add-product`, product);

  return response.data;
};

export const usePostProduct = () => {
  return useMutation<ApiProductPost['post']['responseData'], DefaultError, ApiProductPost['post']['variables']>({
    mutationFn: postProduct,
  });
};
