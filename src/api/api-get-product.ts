import axiosInstance from '@api/axiosInstance';
import { API_URL } from '@constants';
import { MixingRate, Product, Term } from '@custom-types';
import { DefaultError, useMutation, useQuery } from '@tanstack/react-query';
import { AxiosError, AxiosResponse } from 'axios';

export type ApiProduct = {
  get: {
    responseData: {
      code: string;
      data: Product;
      message: string;
    };
  };
  post: {};
};

// get product
export const getProduct = async (product_id: string) => {
  const response = await axiosInstance.get<
    ApiProduct['get']['responseData'],
    AxiosResponse<ApiProduct['get']['responseData']>
  >(`${API_URL}/products/get-product?product_id=${product_id}`);

  return response.data;
};

export const useGetProduct = (product_id: string) =>
  useQuery<ApiProduct['get']['responseData']>({
    queryKey: ['get-product', product_id],
    queryFn: () => getProduct(product_id),
  });

export default useGetProduct;

export type ApiProducts = {
  get: {
    params: {
      page: number;
      postsPerPage: number;
    };
    responseData: {
      code: string;
      data: Array<Product>;
      message: string;
      current_page: number;
      total_posts: number;
    };
  };
};

export const getProducts = async ({ page, postsPerPage }: ApiProducts['get']['params']) => {
  const response = await axiosInstance.get<
    ApiProducts['get']['responseData'],
    AxiosResponse<ApiProducts['get']['responseData']>
  >(`${API_URL}/products/get-products?page=${page}&posts_per_page=${postsPerPage}`);

  return response.data;
};

export const useGetProducts = ({ page, postsPerPage }: ApiProducts['get']['params']) =>
  useQuery<ApiProducts['get']['responseData']>({
    queryKey: ['get-products', page, postsPerPage],
    queryFn: () => getProducts({ page, postsPerPage }),
  });
