import axiosInstance from '@api/axiosInstance';
import { API_URL } from '@constants';
import { DefaultError, useMutation, useQuery } from '@tanstack/react-query';
import { AxiosError, AxiosResponse } from 'axios';

export type ApiMyAccount = {
  get: {
    responseData: {
      code: string;
      message: string;
      data: {
        status: number;
        user_id: string;
        email: string;
        brand_id: string;
        brand_name: string;
        building: string;
        floor: string;
        room_number: string;
        deposit_account: string;
        bank: string;
        phone_number: string;
      };
    };
  };
  post: {
    params: {
      email: string;
      password_1: string;
      password_2: string;
      deposit_account: string;
      brand: string;
      phone_number: string;
    };
    responseData: {
      code: string;
      message: string;
      data: {
        code: string;
        message: string;
        data: any;
      };
    };
    variables: ApiMyAccount['post']['params'];
  };
};

// get myaccount
export const getMyAccount = async () => {
  const response = await axiosInstance.get<
    ApiMyAccount['get']['responseData'],
    AxiosResponse<ApiMyAccount['get']['responseData']>
  >(`${API_URL}/my-account/get-my-account`);

  console.log('response.data', response.data);

  return response.data;
};

export const useGetMyAccount = () =>
  useQuery<ApiMyAccount['get']['responseData']>({
    queryKey: ['get-my-account'],
    queryFn: getMyAccount,
  });

// post myaccount
export const postMyAccount = async (variables: ApiMyAccount['post']['variables']) => {
  const response = await axiosInstance.post<
    ApiMyAccount['post']['responseData'],
    AxiosResponse<ApiMyAccount['post']['responseData']>
  >(`${API_URL}/my-account/update-my-account`, variables);

  return response.data;
};

export const usePostMyAccount = () =>
  useMutation<ApiMyAccount['post']['responseData'], AxiosError<DefaultError>, ApiMyAccount['post']['variables']>({
    mutationFn: postMyAccount,
  });
