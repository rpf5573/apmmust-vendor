import axiosInstance from '@api/axiosInstance';
import { API_URL } from '@constants';
import { CategoryTerm, Term, TermMap } from '@custom-types';
import { DefaultError, useMutation, useQuery } from '@tanstack/react-query';
import { transformGender } from '@utils';
import { AxiosError, AxiosResponse } from 'axios';

export type ApiTerms = {
  get: {
    responseData: {
      code: string;
      message: string;
      data: {
        brands: Array<Term>;
        categories: Array<CategoryTerm>;
        colours: Array<Term>;
        genders: Array<Term>;
        made_ins: Array<Term>;
        seasons: Array<Term>;
        sizes: Array<Term>;
        updates: Array<Term>;
        // MAPS
        brand_map: TermMap;
        category_map: TermMap;
        colour_map: TermMap;
        gender_map: TermMap;
        made_in_map: TermMap;
        season_map: TermMap;
        size_map: TermMap;
        update_map: TermMap;
      };
    };
  };
};

// login
const getProductTerms = async () => {
  const response = await axiosInstance.get<
    ApiTerms['get']['responseData'],
    AxiosResponse<ApiTerms['get']['responseData']>
  >(`${API_URL}/products/get-all-product-terms`);

  if (response.data && response.data.data) {
    const genders = response.data.data.genders;
    for (let i = 0; i < genders.length; i++) {
      genders[i].slug = transformGender(genders[i].slug ?? '');
    }
    response.data.data.genders = genders;
  }

  return response.data;
};

export const useGetProductTerms = () =>
  useQuery<ApiTerms['get']['responseData']>({
    queryKey: ['get-all-product-terms'],
    queryFn: getProductTerms,
  });

export default useGetProductTerms;
