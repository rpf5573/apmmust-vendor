import axiosInstance from '@api/axiosInstance';
import { API_URL } from '@constants';
import type { DefaultError } from '@tanstack/query-core';
import { useMutation } from '@tanstack/react-query';
import { AxiosError, AxiosResponse } from 'axios';

export type ApiSignup = {
  post: {
    params: {
      email: string;
      password_1: string;
      password_2: string;
      deposit_account: string;
      brand: string;
      phone_number: string;
    };
    responseData: {
      code: string;
      message: string;
      data: {
        status: number;
        user_id: number;
      };
    };
    variables: ApiSignup['post']['params'];
  };
};

export const postSignup = async (params: ApiSignup['post']['variables']) => {
  const response = await axiosInstance.post<
    ApiSignup['post']['responseData'],
    AxiosResponse<ApiSignup['post']['responseData']>,
    ApiSignup['post']['variables']
  >(`${API_URL}/auth/signup`, params);

  return response.data;
};

export const usePostSignup = () =>
  useMutation<ApiSignup['post']['responseData'], DefaultError, ApiSignup['post']['variables']>({
    mutationFn: postSignup,
  });

export default usePostSignup;
