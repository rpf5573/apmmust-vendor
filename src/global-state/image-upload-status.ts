import { atom } from 'jotai';

const atom_imageUploadStatus = atom<{
  isFeaturedImageUploading?: boolean;
  isGalleryImageUploading?: boolean;
}>({
  isFeaturedImageUploading: false,
  isGalleryImageUploading: false,
});

export default atom_imageUploadStatus;
